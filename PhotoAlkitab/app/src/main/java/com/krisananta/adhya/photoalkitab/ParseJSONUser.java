package com.krisananta.adhya.photoalkitab;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Adhya Krisananta on 15/04/2016.
 */
public class ParseJSONUser {
//    public static String[] ids;
//    public static String[] usernames;
//    public static String[] passwords;

    private static String ids;
    private static String usernames;
    private static String passwords;
    private static String tokens;

    public static final String JSON_ARRAY = "result";
    public static final String KEY_ID = "id";
    public static final String KEY_NAME = "username";
    public static final String KEY_EMAIL = "password";

    private JSONArray users = null;

    private String json;

    public ParseJSONUser(String json){
        this.json = json;
    }

    protected void parseJSON(){
        JSONObject jsonObject=null;
        try {
            jsonObject = new JSONObject(json);
//            users = jsonObject.getJSONArray(JSON_ARRAY);
//
//            ids = new String[users.length()];
//            usernames = new String[users.length()];
//            passwords = new String[users.length()];
//
//            for(int i=0;i<users.length();i++){
//                JSONObject jo = users.getJSONObject(i);
//                ids[i] = jo.getString(KEY_ID);
//                usernames[i] = jo.getString(KEY_NAME);
//                passwords[i] = jo.getString(KEY_EMAIL);
//            }
//            JSONObject jsonO = jsonObject.getJSONObject(json);
            ids = jsonObject.getString("id");
            usernames = jsonObject.getString("username");
//            passwords = jsonObject.getString("password");
            tokens = jsonObject.getString("token");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static String getIds() {
        return ids;
    }

    public static String getUsernames() {
        return usernames;
    }

    public static String getPasswords() {
        return passwords;
    }

    public static String getTokens() {
        return tokens;
    }
}
