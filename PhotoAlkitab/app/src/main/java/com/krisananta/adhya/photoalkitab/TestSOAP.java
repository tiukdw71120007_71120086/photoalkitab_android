package com.krisananta.adhya.photoalkitab;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import junit.framework.Test;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

public class TestSOAP extends AppCompatActivity {

    EditText txtInput;
    RadioGroup radioGroupChooser;
    RadioButton radioCelsius, radioFahrenheit, radioTemp;
    Button btnConvert;
    String tempValue;
    ProgressDialog pDialog;
    SoapObject request;
    SoapPrimitive fahtocel, celtofah;

    String METHOD_NAME1 = "CelsiusToFahrenheit";
    String METHOD_NAME2 = "FahrenheitToCelcius";
    String SOAP_ACTION1 = "http://www.w3schools.com/xml/CelsiusToFahrenheit";
    String SOAP_ACTION2 = "http://www.w3schools.com/xml/FahrenheitToCelcius";

    String NAMESPACE = "http://www.w3schools.com/xml/";
    String SOAP_URL =  "http://www.w3schools.com/xml/tempconvert.asmx";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_soap);

        txtInput = (EditText)findViewById(R.id.txtInput);
        radioGroupChooser = (RadioGroup)findViewById(R.id.radioGroupChooser);
        radioCelsius = (RadioButton)findViewById(R.id.radioCelsius);
        radioFahrenheit = (RadioButton)findViewById(R.id.radioFahrenheit);
        btnConvert = (Button)findViewById(R.id.btnConvert);


    }

    public void onClick(View v){
        int temp = radioGroupChooser.getCheckedRadioButtonId();
        radioTemp = (RadioButton)findViewById(temp);
        tempValue = txtInput.getText().toString();
        String tempChoose = radioTemp.getText().toString();

        switch (v.getId()){
            case R.id.btnConvert:
                if (isEmpty(txtInput)){
                    Toast.makeText(TestSOAP.this, "Enter degress to convert!", Toast.LENGTH_SHORT).show();
                }else if (tempChoose.equals("Celsius")){
                    CelsiusAsync celsiusToFahr = new CelsiusAsync();
                    celsiusToFahr.execute();
                }else if (tempChoose.equals("Fahrenheit")){

                }
        }
    }

    public Boolean isEmpty(EditText txtInput){
        String noValue = txtInput.getText().toString().trim();
        if (noValue.length()==0){
            return true;
        }
        else{
            return false;
        }
    }

    private class CelsiusAsync extends AsyncTask<Void,Void,Void>{
        @Override
        protected Void doInBackground(Void... params) {
            request = new SoapObject(NAMESPACE, METHOD_NAME1);
//            request = new SoapObject(METHOD_NAME1, NAMESPACE);
            request.addProperty("Celsius", tempValue);
            System.out.println("tempValue = "+tempValue);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet=true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_URL);
            try {
                httpTransport.call(SOAP_ACTION1, envelope);
                fahtocel = (SoapPrimitive)envelope.getResponse();
//                Log.d("fahrenheit", fahtocel.toString());
                System.out.println("fahrenheit = "+fahtocel);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(TestSOAP.this);
            pDialog.setMessage("Converting...");
            pDialog.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            pDialog.dismiss();
            Toast.makeText(TestSOAP.this, fahtocel.toString()+" Fahrenheit", Toast.LENGTH_SHORT).show();
        }
    }
}