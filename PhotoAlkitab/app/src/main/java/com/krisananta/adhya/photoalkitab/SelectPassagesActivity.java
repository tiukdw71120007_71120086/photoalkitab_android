package com.krisananta.adhya.photoalkitab;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Base64;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SelectPassagesActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    Spinner spinnerBook, spinnerChapters, spinnerVerses;
    TextView lblPassage, lblPilihBab, lblPasal;
    String responseOrError,book,chapter,verse,bookName;
    ProgressBar mProgressBook, mProgressChapters, mProgressVerses, mProgressPassage;
    Button btnOK, btnCancel;

    List<String> listBook = new ArrayList<String>();
    List<String> listChapter = new ArrayList<String>();
    List<String> listVerses = new ArrayList<String>();
    Map<String, String> listBooks = new HashMap<String, String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_passages);

        lblPilihBab = (TextView)findViewById(R.id.lblPilihBab);
        lblPasal = (TextView)findViewById(R.id.lblPasal);
        lblPassage = (TextView)findViewById(R.id.lblAyat);

        spinnerBook = (Spinner)findViewById(R.id.spinnerBook);
        spinnerChapters = (Spinner)findViewById(R.id.spinnerBab);
        spinnerVerses = (Spinner)findViewById(R.id.spinnerPasal);

        mProgressBook = (ProgressBar)findViewById(R.id.progressBarBook);
        mProgressChapters = (ProgressBar)findViewById(R.id.progressBarBab);
        mProgressVerses = (ProgressBar)findViewById(R.id.progressBarPasal);
        mProgressPassage = (ProgressBar)findViewById(R.id.progressBarAyat);


        lblPilihBab.setVisibility(View.GONE);
        lblPasal.setVisibility(View.GONE);
        lblPassage.setVisibility(View.GONE);
        spinnerBook.setVisibility(View.GONE);
        spinnerChapters.setVisibility(View.GONE);
        spinnerVerses.setVisibility(View.GONE);
        mProgressPassage.setVisibility(View.GONE);
        mProgressChapters.setVisibility(View.GONE);
        mProgressVerses.setVisibility(View.GONE);

        getBook();
//        getChapters("2Tim");

        btnOK =(Button)findViewById(R.id.btnOK);
        btnOK.setEnabled(false);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("result", lblPassage.getText().toString());
                intent.putExtra("bookName", bookName);
                intent.putExtra("book", book);
                intent.putExtra("chapter", chapter);
                intent.putExtra("verse", verse);
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });

        btnCancel = (Button)findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                setResult(Activity.RESULT_CANCELED, intent);
                finish();
            }
        });
    }

    public void getBook(){
        String urlAddress = "https://bibles.org/v2/versions/eng-GNTD/books.js";

        final String username = "cKaMAGU4fL3kc4w5zMZbvoPvsmOJEpTjyFUjXeab";
        final String passwd = "x";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, urlAddress, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONObject jsonResponse = response.getJSONObject("response");
                    JSONArray jsonBooks = jsonResponse.getJSONArray("books");
                    for (int i = 0; i<jsonBooks.length(); i++){
                        JSONObject jsonBook = jsonBooks.getJSONObject(i);
                        listBook.add(jsonBook.getString("name"));
                        listBooks.put(jsonBook.getString("name"), jsonBook.getString("abbr"));
                    }

                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i<listBook.size(); i++){
                        sb.append(listBook.get(i)).append("\n");
                    }

                    spinnerBook.setVisibility(View.VISIBLE);
                    spinnerBook.setOnItemSelectedListener(SelectPassagesActivity.this);
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(SelectPassagesActivity.this, android.R.layout.simple_spinner_dropdown_item, listBook);
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerBook.setAdapter(dataAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mProgressBook.setVisibility(View.GONE);
//                lblPilihBab.setVisibility(View.VISIBLE);
//                mProgressChapters.setVisibility(View.VISIBLE);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                responseOrError = error.getMessage();
                Toast.makeText(SelectPassagesActivity.this, "Error\n"+error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                return params;
            }

            @Override
            public Map<String, String> getHeaders(){
                Map<String,String> headers = new HashMap<String, String>();
                String credentials = username+":"+passwd;
                String auth = "Basic " + Base64.encodeToString(credentials.getBytes(),Base64.DEFAULT).replace("\n","");
                headers.put("Authorization", auth);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonObjectRequest);
    }

    public void getChapters(String a){
        String urlAddress = "https://bibles.org/v2/books/eng-GNTD:"+a+"/chapters.js";

        final String username = "cKaMAGU4fL3kc4w5zMZbvoPvsmOJEpTjyFUjXeab";
        final String passwd = "x";

        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, urlAddress, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (listChapter.size()>0){
                    listChapter.clear();
                }
                try {
                    JSONObject jsonResponse = response.getJSONObject("response");
                    JSONArray jsonArray = jsonResponse.getJSONArray("chapters");
                    for (int i=0; i<jsonArray.length(); i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        listChapter.add(jsonObject.getString("chapter"));
                    }
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i<listChapter.size(); i++){
                        sb.append(listChapter.get(i)).append("\n");
                    }

//                    lblPassage.setText(sb);
                    spinnerChapters.setOnItemSelectedListener(SelectPassagesActivity.this);
                    ArrayAdapter<String> dataAdapterChapters = new ArrayAdapter<String>(SelectPassagesActivity.this, android.R.layout.simple_spinner_dropdown_item, listChapter);
                    dataAdapterChapters.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerChapters.setAdapter(dataAdapterChapters);
                    spinnerChapters.setVisibility(View.VISIBLE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mProgressChapters.setVisibility(View.GONE);
//                mProgressVerses.setVisibility(View.VISIBLE);
                lblPilihBab.setVisibility(View.VISIBLE);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                responseOrError = error.getMessage();
                Toast.makeText(SelectPassagesActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                return params;
            }

            @Override
            public Map<String, String> getHeaders(){
                Map<String,String> headers = new HashMap<String, String>();
                String credentials = username+":"+passwd;
                String auth = "Basic " + Base64.encodeToString(credentials.getBytes(),Base64.DEFAULT).replace("\n","");
                headers.put("Authorization", auth);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonObjectRequest);
    }

    public void getVerses(String book, String chapter){
        String urlAddress = "https://bibles.org/v2/chapters/eng-GNTD:"+book+"."+chapter+"/verses.js";

        final String username = "cKaMAGU4fL3kc4w5zMZbvoPvsmOJEpTjyFUjXeab";
        final String passwd = "x";

        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, urlAddress, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (listVerses.size()>0){
                    listVerses.clear();
                }

                try {
                    JSONObject jsonResponse = response.getJSONObject("response");
                    JSONArray jsonVerses = jsonResponse.getJSONArray("verses");
                    for (int i=0; i<jsonVerses.length(); i++){
                        JSONObject jsonVerse = jsonVerses.getJSONObject(i);
                        listVerses.add(jsonVerse.getString("verse"));
                    }

                    spinnerVerses.setOnItemSelectedListener(SelectPassagesActivity.this);
                    ArrayAdapter<String> dataAdapterVerses = new ArrayAdapter<String>(SelectPassagesActivity.this, android.R.layout.simple_spinner_dropdown_item, listVerses);
                    dataAdapterVerses.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerVerses.setAdapter(dataAdapterVerses);
                    spinnerVerses.setVisibility(View.VISIBLE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                lblPasal.setVisibility(View.VISIBLE);
                mProgressVerses.setVisibility(View.GONE);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                responseOrError = error.getMessage();
                Toast.makeText(SelectPassagesActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                return params;
            }

            @Override
            public Map<String, String> getHeaders(){
                Map<String,String> headers = new HashMap<String, String>();
                String credentials = username+":"+passwd;
                String auth = "Basic " + Base64.encodeToString(credentials.getBytes(),Base64.DEFAULT).replace("\n","");
                headers.put("Authorization", auth);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonObjectRequest);
    }

    public void getPassage(String book, String chapter, String verse){
        String urlAddress = "https://bibles.org/v2/passages.js?q[]="+book+"+"+chapter+":"+verse+"&version=eng-GNTD";

        final String username = "cKaMAGU4fL3kc4w5zMZbvoPvsmOJEpTjyFUjXeab";
        final String passwd = "x";

        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, urlAddress, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                String text = null;
                try {
                    JSONObject jsonResponse = response.getJSONObject("response");
                    JSONObject jsonSearch = jsonResponse.getJSONObject("search");
                    JSONObject jsonResult = jsonSearch.getJSONObject("result");
                    JSONArray jsonPassages = jsonResult.getJSONArray("passages");

                    for (int i = 0; i < jsonPassages.length(); i++){
                        JSONObject passages = jsonPassages.getJSONObject(i);
                        text = passages.getString("text");
                    }
                    Document documentNormalization = Jsoup.parse(text);
                    String stringNormalization = documentNormalization.toString();
                    String stringNormalization2 = Html.fromHtml(text).toString();

//                    Toast.makeText(SelectPassagesActivity.this, stringNormalization, Toast.LENGTH_SHORT).show();
                    lblPassage.setText(stringNormalization2);
                    lblPassage.setVisibility(View.VISIBLE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mProgressPassage.setVisibility(View.GONE);
//                lblPasal.setVisibility(View.VISIBLE);
//                lblPassage.setVisibility(View.VISIBLE);
                btnOK.setEnabled(true);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                responseOrError = error.getMessage();
                Toast.makeText(SelectPassagesActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                return params;
            }

            @Override
            public Map<String, String> getHeaders(){
                Map<String,String> headers = new HashMap<String, String>();
                String credentials = username+":"+passwd;
                String auth = "Basic " + Base64.encodeToString(credentials.getBytes(),Base64.DEFAULT).replace("\n","");
                headers.put("Authorization", auth);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonObjectRequest);
    }

    public void getPassagesBibles(){
        String urlAddress = "https://bibles.org/v2/passages.js?q[]=john+3:5&version=eng-GNTD";

        final String username = "cKaMAGU4fL3kc4w5zMZbvoPvsmOJEpTjyFUjXeab";
        final String passwd = "x";


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, urlAddress, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                String text = null;
                try {
                    JSONObject jsonResponse = response.getJSONObject("response");
                    JSONObject jsonSearch = jsonResponse.getJSONObject("search");
                    JSONObject jsonResult = jsonSearch.getJSONObject("result");
                    JSONArray jsonPassages = jsonResult.getJSONArray("passages");

                    for (int i = 0; i < jsonPassages.length(); i++){
                        JSONObject passages = jsonPassages.getJSONObject(i);
                        text = passages.getString("text");
                    }

                    String coba = ">";
                    for (int i = 0; i<3; i++){
                        int indexString = text.indexOf(coba)+1;
                        text = text.substring(indexString);
                    }

                    String tandaDiAkhir = "<";
                    int indexString = text.indexOf(tandaDiAkhir);
                    text = text.substring(0, indexString);

//                    Toast.makeText(SelectPassagesActivity.this, text, Toast.LENGTH_SHORT).show();
                    lblPassage.setText(text);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                responseOrError = error.getMessage();
                Toast.makeText(SelectPassagesActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                return params;
            }

            @Override
            public Map<String, String> getHeaders(){
                Map<String,String> headers = new HashMap<String, String>();
                String credentials = username+":"+passwd;
                String auth = "Basic " + Base64.encodeToString(credentials.getBytes(),Base64.DEFAULT).replace("\n","");
                headers.put("Authorization", auth);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonObjectRequest);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // On selecting a spinner item
//        String item = parent.getItemAtPosition(position).toString();

        // Showing selected spinner item
//        Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();

        Spinner spinner = (Spinner)parent;
//        Toast.makeText(SelectPassagesActivity.this, "id" + spinner.getId(), Toast.LENGTH_SHORT).show();
        if (spinner.getId()==R.id.spinnerBook){
            String item = parent.getItemAtPosition(position).toString();
            book = listBooks.get(item);
            bookName = listBook.get(position);
//            Toast.makeText(SelectPassagesActivity.this, bookName+" - "+book, Toast.LENGTH_SHORT).show();
            getChapters(listBooks.get(item));
            mProgressChapters.setVisibility(View.VISIBLE);
            mProgressVerses.setVisibility(View.GONE);
            mProgressPassage.setVisibility(View.GONE);
            spinnerChapters.setVisibility(View.GONE);
            spinnerVerses.setVisibility(View.GONE);
            lblPilihBab.setVisibility(View.GONE);
            lblPassage.setVisibility(View.GONE);
            lblPasal.setVisibility(View.GONE);
            btnOK.setEnabled(false);
        }else if(spinner.getId()==R.id.spinnerBab){
            String item = parent.getItemAtPosition(position).toString();
            chapter = item;
            if (chapter.equalsIgnoreCase("int")){
                mProgressVerses.setVisibility(View.GONE);
            }else{
                getVerses(book, chapter);
                mProgressVerses.setVisibility(View.VISIBLE);
            }
            mProgressPassage.setVisibility(View.GONE);
            spinnerVerses.setVisibility(View.GONE);
            lblPasal.setVisibility(View.GONE);
            lblPassage.setVisibility(View.GONE);
            btnOK.setEnabled(false);
        }else if(spinner.getId()==R.id.spinnerPasal){
            verse = parent.getItemAtPosition(position).toString();
            getPassage(book, chapter, verse);
//            Toast.makeText(SelectPassagesActivity.this, bookName+" - "+book+" "+chapter+":"+verse, Toast.LENGTH_SHORT).show();
            mProgressPassage.setVisibility(View.VISIBLE);
            lblPassage.setVisibility(View.GONE);
            btnOK.setEnabled(false);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }
}
