package com.krisananta.adhya.photoalkitab;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;


public class TestXMLActivity extends AppCompatActivity {

    Button btnSignature;
    TextView lblXML, lblDigitalSignature;
    ImageView imgExample;

    StringBuilder text;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    public static final String MyPREFERENCES = "MyPrefs";
    public static final String Id = "id";
    public static final String Username = "username";
    public static final String Password = "password";
    public static final String PRIVATEKEY = "privateKey";

    boolean choser1 = true;

    String stringSignature;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_xml);

        btnSignature = (Button) findViewById(R.id.btnSignature);
        lblXML = (TextView) findViewById(R.id.lblXML);
        lblDigitalSignature = (TextView) findViewById(R.id.lblDigitalSignature);
        imgExample = (ImageView)findViewById(R.id.imgExample);

        sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        loadXML2();

        btnSignature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                messageDigest();
//                if (choser1){
                digitalSignature();
//                    choser1 = false;
//                }else{
//                    editXML();
//                    choser1 = true;
//                }
            }
        });

//        lblDigitalSignature.setText(sharedPreferences.getString(PRIVATEKEY, null));
//        checkPrivateKey();
        checkSignature();
    }

    public void loadXML2() {
        File root = new File(Environment.getExternalStorageDirectory(), "photoAlkitab");
        File file = new File(root, "RDF example.xml");

        text = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                if (line.contains("<rdf:Description")){
                    text.append(line);
                    text.append('\n');
                    while ((line = br.readLine()) != null){
                        text.append(line);
                        text.append('\n');
                        if (line.contains("</rdf:Description>")){
                            break;
                        }
                    }
                }

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        lblXML.setText(text.toString());

    }

    public void digitalSignature() {
        File root = new File(Environment.getExternalStorageDirectory(), "photoAlkitab");
        File file = new File(root, "RDF example.xml");

        byte[] privateKeyBytes;
        privateKeyBytes = Base64.decode(sharedPreferences.getString(PRIVATEKEY, null), 0);

        try {
            KeyFactory kf = KeyFactory.getInstance("RSA");
            PrivateKey pk = kf.generatePrivate(new PKCS8EncodedKeySpec(privateKeyBytes));

//            Signature signature = Signature.getInstance("MD5WithRSA");
            Signature signature = Signature.getInstance("SHA1WithRSA");
            signature.initSign(pk);

            FileInputStream fileInputStream = new FileInputStream(file);
            BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);

            byte[] buffer = new byte[2048];
            int len;

            while ((len = bufferedInputStream.read(buffer)) >= 0) {
                signature.update(buffer, 0, len);
            }

            byte[] realSignature = signature.sign();

            lblDigitalSignature.setText(new String(Base64.encode(realSignature, 1)));

            this.stringSignature= new String(Base64.encode(realSignature, 1));

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            lblDigitalSignature.setText(e.getMessage());
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
            lblDigitalSignature.setText(e.getMessage());
        } catch (InvalidKeyException e) {
            e.printStackTrace();
            lblDigitalSignature.setText(e.getMessage());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            lblDigitalSignature.setText(e.getMessage());
        } catch (SignatureException e) {
            e.printStackTrace();
            lblDigitalSignature.setText(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            lblDigitalSignature.setText(e.getMessage());
        }
        newXml();
    }

//    public void editXML() {
//        File root = new File(Environment.getExternalStorageDirectory(), "photoAlkitab");
//        File file = new File(root, "RDF example.xml");
//
//        XPathFactory factory = XPathFactory.newInstance();
//        XPath xPath = factory.newXPath();
//        try {
//            /*
//            DocumentBuilderFactory bf = DocumentBuilderFactory.newInstance();
//            DocumentBuilder builder = bf.newDocumentBuilder();
//            FileInputStream fileInputStream = new FileInputStream(file);
//            Document document = builder.parse(fileInputStream);
//            String xml = "";
//            String desc = xPath.compile("/*[local-name()='RDF']/*[local-name()='Description']/bible").evaluate(document);
//            Toast.makeText(this, desc, Toast.LENGTH_LONG).show();
//            lblDigitalSignature.setText(desc);
//            */
//
//            DocumentBuilderFactory bf = DocumentBuilderFactory.newInstance();
//            DocumentBuilder builder = bf.newDocumentBuilder();
//            FileInputStream fileInputStream = new FileInputStream(file);
//            Document document = builder.parse(fileInputStream);
//
//            NodeList nodes = (NodeList) xPath.evaluate("/*[local-name()='RDF']", document, XPathConstants.NODESET);
//
//            StringBuilder stringBuiler = new StringBuilder();
//
//            for (int i = 0; i<nodes.getLength();i++){
//                Node noded = nodes.item(i);
//                stringBuiler.append(noded.getTextContent());
////                String a = noded.getTextContent();
////                Toast.makeText(TestXMLActivity.this, a, Toast.LENGTH_SHORT).show();
//            }
//            lblDigitalSignature.setText(stringBuiler);
//
//            /*
//            StringBuilder stringBuilder = new StringBuilder();
//            Node node = (Node) xPath.evaluate("/*[local-name()='RDF']/*[local-name()='Description']", document, XPathConstants.NODE);
//            NodeList nodeList = null;
//            if (null != node){
//                nodeList = node.getChildNodes();
//                for (int i = 0; null != nodeList && i < nodeList.getLength(); i++){
//                    Node nod = nodeList.item(i);
//                    if (nod.getNodeType()==Node.ELEMENT_NODE){
//                        stringBuilder.append(nodeList.item(i).getNodeName()+" : "+nod.getFirstChild().getNodeValue());
//                    }
//                }
//            }
//            lblDigitalSignature.setText(stringBuilder);
//            */
//
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (Exception e){
//        }
//
//    }

    public void newXml(){
        String digest = messageDigest();
        StringBuilder newXML=new StringBuilder();
        text = new StringBuilder();

        newXML.append(
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "\n" +
                "<rdf:RDF \n" +
                "xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n" +
                "xmlns:imageObject=\"http://schema.org/ImageObject\"\n" +
                ">\n" +
                "\n" +
                "<rdf:Description rdf:about=\"_MG_1283\">"
        );

        File root = new File(Environment.getExternalStorageDirectory(), "photoAlkitab");
        File file = new File(root, "RDF example.xml");

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            while ((line = br.readLine()) != null) {
                if (line.contains("<rdf:Description")){
                    text.append(line);
                    text.append('\n');
                    while ((line = br.readLine()) != null){
                        text.append(line);
                        text.append('\n');
                        if (line.contains("</rdf:Description>")){
                            break;
                        }
                    }
                }
            }

            newXML.append(
                    text
            );
            newXML.append(
                    "\t<Signature xmlns=\"http://www.w3.org/2000/09/xmldsig#\">\n" +
                    "\t\t<SignedInfo>\n" +
                    "\t\t\t<CanonicalizationMethod Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\"/> \n" +
                    "\t\t\t<SignatureMethod Algoritm=\"http://www.w3.org/2000/09/xmldsig#rsa-sha1\"/>\n" +
                    "\t\t\t<Reference URI=\"http://www.w3.org/TR/2000/REC-xhtml1-20000126/\">\n" +
                    "\t\t\t\t<Transforms>\n" +
                    "\t\t\t\t\t<Transform Algorithm=\"http://www.w3.org/2000/09/xmldsig#enveloped-signature\"/>\n" +
                    "\t\t\t\t</Transforms>\n" +
                    "\t\t\t\t<DigestMethod Algorithm=\"http://www.w3.org/2001/04/xmldsig-more#md5\"/>\n" +
                    "\t\t\t\t<DigestValue>\n" +
                    "\t\t\t\t\t" + digest + "\n" +
                    "\t\t\t\t</DigestValue>\n" +
                    "\t\t\t</Reference>\n" +
                    "\t\t</SignedInfo>\n" +
                    "\t\t<SignatureValue>\n" +
                    "\t\t\t" + stringSignature + "\n" +
                    "\t\t</SignatureValue>\n" +
                    "\t</Signature>\n" +
                    "</rdf:RDF>"
            );

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        lblDigitalSignature.setText(newXML);

//        HASIL newXML DIBUAT MENJADI FILE XML
        try {
            if (!root.exists()){
                root.mkdirs();
            }
            File filepath = new File(root,"newXML.xml");
            FileWriter writer = new FileWriter(filepath);
            writer.append(newXML);
            writer.flush();
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
//        HASIL newXML DIBUAT MENJADI FILE XML
    }

    public String messageDigest(){
        File root = new File(Environment.getExternalStorageDirectory(), "photoAlkitab");
        File file = new File(root, "RDF example.xml");

        text = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                if (line.contains("<rdf:Description")){
                    text.append(line);
                    text.append('\n');
                    while ((line = br.readLine()) != null){
                        text.append(line);
                        text.append('\n');
                        if (line.contains("</rdf:Description>")){
                            break;
                        }
                    }
                }

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String textXML = text.toString();

        //Create MD5 Hash
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        digest.update(textXML.getBytes());
        byte messageDigestByte[]= digest.digest();

        //Create Hex String
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i<messageDigestByte.length; i++){
            hexString.append(Integer.toHexString(0xFF & messageDigestByte[i]));
        }

//        lblDigitalSignature.setText(hexString);
        return hexString.toString();

    }

    public void checkPrivateKey(){

        File root = new File(Environment.getExternalStorageDirectory(), "photoAlkitab");
        File file = new File(root, sharedPreferences.getString(Username, null)+".txt");

        StringBuilder text = new StringBuilder();

        try{
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            while ((line=br.readLine())!=null){
                text.append(line);
                text.append("\n");
            }
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String a = text.toString();
//        String a = sharedPreferences.getString(PRIVATEKEY, null);
        String b = null;

        byte[] privateKeyBytes, privKeyByte;
        privateKeyBytes = Base64.decode(a,0);
        KeyFactory kf = null;
        try {
            kf = KeyFactory.getInstance("RSA");
            PrivateKey pk = kf.generatePrivate(new PKCS8EncodedKeySpec(privateKeyBytes));
            privKeyByte = pk.getEncoded();
            b = new String(Base64.encode(privKeyByte,0));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }

        String c = b.replaceAll("\n","");

        if (a.equals(b)){
            Toast.makeText(TestXMLActivity.this, "a b TRUE", Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(TestXMLActivity.this, "a b FALSE", Toast.LENGTH_LONG).show();
        }
        if (a.equals(c)){
            Toast.makeText(TestXMLActivity.this, "a c TRUE", Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(TestXMLActivity.this, "a c FALSE", Toast.LENGTH_LONG).show();
        }
        lblDigitalSignature.setText(a+"\n"+a.length()+
                "\n\n--------------------------------------------------------------------------\n\n"+
                b+"\n"+b.length()+
                "\n\n--------------------------------------------------------------------------\n\n"+
                c+"\n"+c.length());
    }

    public void checkSignature(){
//        PUBLIC KEY

        String pubkeyString = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDLZ0cu8Cj+eCVntpz/jfW9gJvut2FUM+dtqsGt\n" +
                "b0E02fssBWZ0MwkaQbFGsARtun9MpWPoqCfh6W2XA5rzfaa0R8N0tTJAzv5UUgsvD4XKbn/9n269\n" +
                "2gwDJNzdlL9FPAjpG80GcbK6FQ2tbas+qP53JMC8W/jFSZBqcYVzq7hu6wIDAQAB\n";
        byte[] pubkeyByte = Base64.decode(pubkeyString, 0);
        KeyFactory keyFactory = null;
        PublicKey publicKey=null;
        try {
            keyFactory = KeyFactory.getInstance("RSA");
            publicKey = keyFactory.generatePublic(new X509EncodedKeySpec(pubkeyByte));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }

//        PUBLIC KEY

//        BASE 64 PICTURE

        String stringRealPath = "/storage/emulated/0/photoAlkitab/contoh.jpg";
        Uri uriImage = Uri.fromFile(new File(stringRealPath));
        Bitmap bitmapImage = null;
        try {
            bitmapImage = BitmapFactory.decodeStream(getContentResolver().openInputStream(uriImage));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        imgExample.setImageBitmap(bitmapImage);

        imgExample.buildDrawingCache();
        bitmapImage=imgExample.getDrawingCache();
        ByteArrayOutputStream byteArrayOutputStreamImage = new ByteArrayOutputStream();
        byte[] byteImage = byteArrayOutputStreamImage.toByteArray();

//        String base64Picture = null;
//        lblDigitalSignature.setText(base64Picture);

        byte[] bytePicture = byteImage;
//        BASE 64 PICTURE

//        BYTE SIGNATURE

        String signatureString = "nD4dVUFi/yfPMhPMiQDR3qHrjpOCzcWkDdRF53RFhKX/j6HcxCk7pIwCWR0oyV+TVxwCGBMo91yi\n" +
                "5Kd8Ebx+n8qktnDxVMkOQzkru9y63loWTaJl8pkuurLOENMCTSJclUtDmJYwvZA6ROHZZJm/NSYW\n" +
                "xI8qiUCdLKDLKet5Qg0=";
        byte[] byteSignature = Base64.decode(signatureString,0);

//        BYTE SIGNATURE

        String stringDigestValue = "70145253940797b24b518347f7b528a752b790f1";
        byte[] byteDigestValue = Base64.decode(stringDigestValue,0);



        try {
            Signature signature = Signature.getInstance("SHA1withRSA");
            signature.initVerify(publicKey);
            signature.update(byteDigestValue);
            boolean ok = signature.verify(byteSignature);
            Toast.makeText(TestXMLActivity.this, ""+ok, Toast.LENGTH_LONG).show();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            e.printStackTrace();
        }
    }
}
