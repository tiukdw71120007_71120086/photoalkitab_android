package com.krisananta.adhya.photoalkitab;

import android.graphics.Bitmap;
import android.os.Environment;
import android.support.v7.widget.LinearLayoutManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Array;
import java.util.ArrayList;
import java.util.List;

import static android.widget.Toast.LENGTH_SHORT;

/**
 * Created by Adhya Krisananta on 12/09/2016.
 */
public class Get_Internal_Data {
    private File root = new File(Environment.getExternalStorageDirectory(),"photoAlkitab/source");
    private String PATH = "/storage/emulated/0/photoAlkitab/source/";
    private ArrayList<String> arrayData = new ArrayList<String>(), list_Data = new ArrayList();
    private String
            filename = "",
            string_Image = "",
            string_Passage = "",
            book = "",
            chapter = "",
            verse = "",
            horizontal_Align = "",
            vertical_Align = "",
            string_Type_Face = "",
            color_Selection = ""
    ;

    private StringBuilder stringBuilder_Passage;

    private Bitmap bitmap = null;
    private int size_Data = 0;


    public ArrayList<String> getListData(){
        size_Data = 0;
        if (!root.exists()){
            ArrayList<String> list_empty = new ArrayList();
            list_empty.add("Data Kosong");
            System.out.println("Data Kosong");
            return list_empty;
        }else{
//            int i
            for (String tempFile : root.list()){
                list_Data.add(tempFile);
                size_Data++;
            }
//            System.out.println(list_Data);
            return list_Data;
        }
    }

    public int getSizeData(){
        size_Data = 0;
        if (!root.exists()){
            size_Data = 0;
            return size_Data;
        }else{
            for (String tempFile : root.list()){
                size_Data++;
            }
//            System.out.println(size_Data);
            return size_Data;
        }
    }

    public ArrayList getSelectedData(String file_Name){
        File file = new File(root, file_Name);

//        String text;

        try{
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            stringBuilder_Passage = new StringBuilder();
            while ((line=br.readLine())!=null){
                if (line.contains("<imageFile>")){
                    this.string_Image = line;
                    System.out.println(string_Image);
                }else if (line.contains("<text>")){
                    stringBuilder_Passage.append(line);
                    stringBuilder_Passage.append("\n");
                    while ((line=br.readLine())!=null && !line.contains("</text>")){
                        stringBuilder_Passage.append(line);
                        stringBuilder_Passage.append("\n");
                    }
                    stringBuilder_Passage.append(line);
//                    this.string_Passage = line;
                }else if (line.contains("<book>")){
                    this.book = line;
                }else if (line.contains("<chapter>")){
                    this.chapter = line;
                }else if (line.contains("<verse>")){
                    this.verse = line;
                }else if (line.contains("<horizontal>")){
                    this.horizontal_Align = line;
                }else if (line.contains("<vertical>")){
                    this.vertical_Align = line;
                }else if (line.contains("<type>")){
                    this.string_Type_Face = line;
                }else if (line.contains("<color>")){
                    this.color_Selection = line;
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        string_Passage = stringBuilder_Passage.toString();
//        System.out.println(this.string_Passage);
        this.string_Image = this.string_Image.replace("<imageFile>","");
        this.string_Image = this.string_Image.replace("</imageFile>", "");
        this.string_Image = this.string_Image.replace("\t", "");

        this.string_Passage = this.string_Passage.replace("<text>","");
        this.string_Passage = this.string_Passage.replace("</text>", "");
        this.string_Passage= this.string_Passage.replace("\t", "");

        this.book = this.book.replace("<book>","");
        this.book = this.book.replace(" ","");
        this.book = this.book.replace("</book>","");
        this.book = this.book.replace("\t", "");

        this.chapter = this.chapter.replace("<chapter>","");
        this.chapter = this.chapter.replace(" ","");
        this.chapter = this.chapter.replace("</chapter>","");
        this.chapter = this.chapter.replace("\t", "");

        this.verse = this.verse.replace("<verse>","");
        this.verse = this.verse.replace(" ","");
        this.verse = this.verse.replace("</verse>","");
        this.verse = this.verse.replace("\t", "");

        this.horizontal_Align = this.horizontal_Align.replace("<horizontal>","");
        this.horizontal_Align = this.horizontal_Align.replace("</horizontal>","");
        this.horizontal_Align = this.horizontal_Align.replace("\t", "");
        this.horizontal_Align = this.horizontal_Align.replace(" ", "");

        this.vertical_Align = this.vertical_Align.replace("<vertical>","");
        this.vertical_Align = this.vertical_Align.replace("</vertical>","");
        this.vertical_Align = this.vertical_Align.replace("\t", "");
        this.vertical_Align = this.vertical_Align.replace(" ", "");

        this.string_Type_Face = this.string_Type_Face.replace("<type>","");
        this.string_Type_Face = this.string_Type_Face.replace("</type>","");
        this.string_Type_Face = this.string_Type_Face.replace("\t", "");
        this.string_Type_Face = this.string_Type_Face.replace(" ", "");

        this.color_Selection = this.color_Selection.replace("<color>", "");
        this.color_Selection = this.color_Selection.replace("</color>", "");
        this.color_Selection = this.color_Selection.replace("\t", "");
        this.color_Selection = this.color_Selection.replace(" ", "");

        ArrayList<String> a = new ArrayList();
        a.add(string_Image);
        a.add(string_Passage);
        a.add(book);
        a.add(chapter);
        a.add(verse);
        a.add(horizontal_Align);
        a.add(vertical_Align);
        a.add(string_Type_Face);
        a.add(color_Selection);
        return a;
    }

}
