package com.krisananta.adhya.photoalkitab;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.Base64;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.w3c.dom.Text;
import org.xmlpull.v1.XmlPullParserException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.sql.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Adhya Krisananta on 15/09/2016.
 */
public class EditPictureActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private Button btnTakeAPicture, btnFromGallery, btnAddAyat, btnBlack, btnWhite, btnRed, btnGreen, btnBlue, btnOk;
    private ImageView imgPicture;
    private String
            textAyat, inputType, colorSelection=null,verticalAlign="middle",
            horizontalAlign="center", realPath, fileNameNew, bookName,
            book, chapter, verse, stringTypeface=null,
            tmpNewXML, stringImageEdit;
    private StringBuilder tmpStringBuilder;
    private Spinner spinnerFontChooser;
    private List<String> fontChooser = new ArrayList<>();
    private TextView txtContoh;
    private Typeface typefaceSelected=null;
    private Bitmap BITMAP=null, editingBITMAP=null;

    private RadioGroup radioGroupVertical, radioGroupHorizontal;
    private RadioButton radioVerticalTop, radioVerticalCenter, radioVerticalBottom, radioHorizontalLeft, radioHorizontalCenter, radioHorizontalRight;

    private File fileTemp = new File(Environment.getExternalStorageDirectory().toString());
    private Uri source1=null;

    private CharSequence[] charSequence = new CharSequence[]{"Simpan", "Hanya Kirim", "Simpan dan Kirim"};

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    public static final String MYPREFERENCES = "MyPrefs";
    public static final String ID = "id";
    public static final String USERNAME= "username";
    public static final String PASSWORD = "password";
    public static final String PRIVATEKEY = "privateKey";
    public static final String TOKEN = "token";

    //    String tag EXIF
    private String tagMake="-", tagModel="-", tagOrientation="-", tagExposureTime="-",
            tagAperture="-", tagISO="-", tagDateTime="-", tagFlash="-",
            tagFocalLength="-", tagImageLength="-", tagImageWidth="-", tagGPSLatitude="-",
            tagGPSLatitudeRef="-", tagGPSLongitude="-", tagGPSLongitudeRef="-", tagWhiteBalance="-";

    //    VARIABLE TO SEND SOAP
    private String strResponse;
    private ProgressDialog pDialog;
    private SoapObject request;
    private SoapPrimitive response;
//    VARIABLE TO SEND SOAP

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_photo);

        txtContoh = (TextView)findViewById(R.id.txtContoh);

        sharedPreferences = getSharedPreferences(MYPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

//        Font chooser initiation
        fontChooser.add("Default");
        fontChooser.add("Helvetica");
        fontChooser.add("Monospace");
        fontChooser.add("Sans Serif");
        fontChooser.add("Serif");
        fontChooser.add("Segoe UI");
        fontChooser.add("Times New Roman");
//        Font chooser initiation

        imgPicture = (ImageView)findViewById(R.id.imgPicture);

//        Button Initiation
        btnTakeAPicture = (Button)findViewById(R.id.btnTakeAPicture);
        btnFromGallery = (Button)findViewById(R.id.btnGallery);
        btnAddAyat = (Button)findViewById(R.id.btnAddAyat);
        btnAddAyat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EditPictureActivity.this, SelectPassagesActivity.class);
                startActivityForResult(intent, 10);
            }
        });
        btnOk = (Button)findViewById(R.id.btnOkSelectPhoto);
//        btnOk.setEnabled(false);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(EditPictureActivity.this);
                alertDialogBuilder.setTitle("Pilih Aktifitas");
                alertDialogBuilder.setItems(charSequence, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
//                        Toast.makeText(AddPhotoActivity.this, dialog + " - " + which, Toast.LENGTH_SHORT).show();
                        if (which == 0){
                            fileName();
                            savePicture();
                            buildXMLtoEditPicture(BITMAP);
                            writeEXIFtoImage(fileNameNew);
                        }else if(which == 1){
                            fileName();
                            writeXML(imgPicture);
                            SendToWebServiceAsync sendIt = new SendToWebServiceAsync();
                            sendIt.execute();
                        }else if(which == 2){

                        }

                    }
                });
                alertDialogBuilder.create().show();

            }
        });
//        Button Initiation

//        Spinner Initiation
        spinnerFontChooser=(Spinner)findViewById(R.id.spinnerFontChooser);
        spinnerFontChooser.setOnItemSelectedListener(this);
        ArrayAdapter arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,fontChooser);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerFontChooser.setAdapter(arrayAdapter);
        spinnerFontChooser.setSelection(0);
//        spinnerFontChooser.setEnabled(false);
//        Spinner Initiation

//        Button Color
        btnBlack = (Button)findViewById(R.id.btnBlack);
        btnBlack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                colorSelection="#000000";
                imgPicture.setImageBitmap(usingMethodCanvas(inputType, textAyat, typefaceSelected, colorSelection, horizontalAlign, verticalAlign));
            }
        });
        btnWhite = (Button)findViewById(R.id.btnWhite);
        btnWhite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                colorSelection="#ffffff";
                imgPicture.setImageBitmap(usingMethodCanvas(inputType, textAyat, typefaceSelected, colorSelection, horizontalAlign, verticalAlign));
            }
        });
        btnRed = (Button)findViewById(R.id.btnRed);
        btnRed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                colorSelection="#ff0000";
                imgPicture.setImageBitmap(usingMethodCanvas(inputType, textAyat, typefaceSelected, colorSelection, horizontalAlign, verticalAlign));
            }
        });
        btnGreen = (Button)findViewById(R.id.btnGreen);
        btnGreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                colorSelection="#00ff00";
                imgPicture.setImageBitmap(usingMethodCanvas(inputType, textAyat, typefaceSelected, colorSelection, horizontalAlign, verticalAlign));
            }
        });
        btnBlue = (Button)findViewById(R.id.btnBlue);
        btnBlue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                colorSelection="#0000ff";
                imgPicture.setImageBitmap(usingMethodCanvas(inputType, textAyat, typefaceSelected, colorSelection, horizontalAlign, verticalAlign));
            }
        });
//        Button Color

//        RadioGroupVertical
        radioVerticalCenter= (RadioButton)findViewById(R.id.radioVerticalCenter);
        radioVerticalCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verticalAlign = "middle";
                imgPicture.setImageBitmap(usingMethodCanvas(inputType, textAyat, typefaceSelected, colorSelection, horizontalAlign, verticalAlign));
            }
        });

        radioVerticalTop = (RadioButton)findViewById(R.id.radioVerticalTop);
        radioVerticalTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verticalAlign = "top";
                imgPicture.setImageBitmap(usingMethodCanvas(inputType, textAyat, typefaceSelected, colorSelection, horizontalAlign, verticalAlign));
            }
        });

        radioVerticalBottom = (RadioButton)findViewById(R.id.radioVerticalBotom);
        radioVerticalBottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verticalAlign = "bottom";
                imgPicture.setImageBitmap(usingMethodCanvas(inputType, textAyat, typefaceSelected, colorSelection, horizontalAlign, verticalAlign));
            }
        });
//        RadioGroupVertical

//        RadioGroupHorizontal
        radioHorizontalLeft=(RadioButton)findViewById(R.id.radioHorizontalLeft);
        radioHorizontalCenter=(RadioButton)findViewById(R.id.radioHorizontalCenter);
        radioHorizontalRight=(RadioButton)findViewById(R.id.radioHorizontalRight);
        radioHorizontalLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                horizontalAlign = "left";
                imgPicture.setImageBitmap(usingMethodCanvas(inputType, textAyat, typefaceSelected, colorSelection, horizontalAlign, verticalAlign));
            }
        });
        radioHorizontalCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                horizontalAlign = "center";
                imgPicture.setImageBitmap(usingMethodCanvas(inputType, textAyat, typefaceSelected, colorSelection, horizontalAlign, verticalAlign));
            }
        });
        radioHorizontalRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                horizontalAlign = "right";
                imgPicture.setImageBitmap(usingMethodCanvas(inputType, textAyat, typefaceSelected, colorSelection, horizontalAlign, verticalAlign));
            }
        });
//        RadioGroupHorizontal


        //------------------------------------------------------------------------------------------
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        final Intent intentFromPrevious = getIntent();
//        System.out.println(intentFromPrevious.getStringExtra("ARRAY SELECTED FILE"));

        final ArrayList<String>[] arrayValueFile = new ArrayList[1];

        fileNameNew = intentFromPrevious.getStringExtra("ARRAY SELECTED FILE");
        System.out.println(fileNameNew);
        fileNameNew = fileNameNew.replace(".xml", "");
        System.out.println(fileNameNew);

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                Get_Internal_Data gid = new Get_Internal_Data();
                arrayValueFile[0] = gid.getSelectedData(intentFromPrevious.getStringExtra("ARRAY SELECTED FILE"));
                for (int i = 0; i< arrayValueFile[0].size(); i++){
                    System.out.println(arrayValueFile[0].get(i));
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        stringImageEdit = arrayValueFile[0].get(0);
        textAyat        = arrayValueFile[0].get(1);
        book            = arrayValueFile[0].get(2);
        chapter         = arrayValueFile[0].get(3);
        verse           = arrayValueFile[0].get(4);
        horizontalAlign = arrayValueFile[0].get(5);
        verticalAlign   = arrayValueFile[0].get(6);
        stringTypeface  = arrayValueFile[0].get(7);
            if (stringTypeface.equalsIgnoreCase("default")){
                spinnerFontChooser.setSelection(0);
                typefaceSelected = Typeface.DEFAULT;
            }else if (stringTypeface.equalsIgnoreCase("Helvetica")){
                spinnerFontChooser.setSelection(1);
                Typeface typefaceNew = Typeface.createFromAsset(getAssets(), "fonts/Helvetica.ttf");
                typefaceSelected = typefaceNew;
            }else if (stringTypeface.equalsIgnoreCase("Monospace")){
                spinnerFontChooser.setSelection(2);
                typefaceSelected = Typeface.MONOSPACE;
            }else if (stringTypeface.equalsIgnoreCase("Sans Serif")){
                typefaceSelected = Typeface.SANS_SERIF;
                spinnerFontChooser.setSelection(3);
            }else if (stringTypeface.equalsIgnoreCase("Segoe UI")){
                Typeface typefaceNew = Typeface.createFromAsset(getAssets(), "fonts/segoeui.ttf");
                typefaceSelected = typefaceNew;
                spinnerFontChooser.setSelection(4);
            }else if (stringTypeface.equalsIgnoreCase("Times New Roman")){
                Typeface typefaceNew = Typeface.createFromAsset(getAssets(), "fonts/times.ttf");
                typefaceSelected = typefaceNew;
                spinnerFontChooser.setSelection(5);
            }
        colorSelection  = arrayValueFile[0].get(8);
        colorSelection.replace("\t","");
        colorSelection.replace(" ","");
        colorSelection.replace("    ", "");

        byte[] decodeString = Base64.decode(stringImageEdit,Base64.DEFAULT);
        Bitmap decodeByte = BitmapFactory.decodeByteArray(decodeString,0,decodeString.length);
        editingBITMAP = decodeByte;
        BITMAP = decodeByte;

        inputType = "edit";
        loadPassage(book, chapter, verse);
        getBookName(book);
        System.out.println(
                "Input Type : "+ inputType + "\n" +
                "Text Ayat = " + textAyat + "\n" +
                "Typeface = " + typefaceSelected + "\n" +
                "Color Selection = " + colorSelection + "\n" +
                "Horizontal = " + horizontalAlign + "\n" +
                "Vertical = " + verticalAlign
        );
        imgPicture.setImageBitmap(usingMethodCanvas(inputType, textAyat, typefaceSelected, colorSelection, horizontalAlign, verticalAlign));

        System.out.println("c");
        progressDialog.dismiss();

        btnAddAyat.setEnabled(true);
        radioVerticalTop.setEnabled(true);
        radioVerticalBottom.setEnabled(true);
        radioVerticalCenter.setEnabled(true);
        radioHorizontalCenter.setEnabled(true);
        radioHorizontalLeft.setEnabled(true);
        radioHorizontalRight.setEnabled(true);
        if (!textAyat.isEmpty()||textAyat!=""){
            btnAddAyat.setText("Ganti Ayat");
        }
        txtContoh.setVisibility(View.INVISIBLE);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Spinner spinner = (Spinner)parent;
        if (spinner.getId() == R.id.spinnerFontChooser){
            String item = parent.getItemAtPosition(position).toString();
            if (item.equalsIgnoreCase("Default")){
                stringTypeface = "Default";
            } else if (!item.equalsIgnoreCase("Default")){
//                fontChooser.add("Default");
                fontChooser.remove("Default");
            }
            if (item.equalsIgnoreCase("monospace")){
                typefaceSelected = Typeface.MONOSPACE;
                stringTypeface = "Monospace";
                imgPicture.setImageBitmap(usingMethodCanvas(inputType, textAyat, typefaceSelected, colorSelection, horizontalAlign, verticalAlign));
            }
            if (item.equalsIgnoreCase("sans serif")){
                typefaceSelected = Typeface.SANS_SERIF;
                stringTypeface = "Sans Serif";
                imgPicture.setImageBitmap(usingMethodCanvas(inputType, textAyat, typefaceSelected, colorSelection, horizontalAlign, verticalAlign));
            }
            if (item.equalsIgnoreCase("serif")){
                typefaceSelected = Typeface.SERIF;
                stringTypeface = "Serif";
                imgPicture.setImageBitmap(usingMethodCanvas(inputType, textAyat, typefaceSelected, colorSelection, horizontalAlign, verticalAlign));
            }
            if (item.equalsIgnoreCase("helvetica")){
                Typeface typefaceNew = Typeface.createFromAsset(getAssets(), "fonts/Helvetica.ttf");
                typefaceSelected = typefaceNew;
                stringTypeface = "Helvetica";
                imgPicture.setImageBitmap(usingMethodCanvas(inputType, textAyat, typefaceSelected, colorSelection, horizontalAlign, verticalAlign));
            }
            if (item.equalsIgnoreCase("times new roman")){
                Typeface typefaceNew = Typeface.createFromAsset(getAssets(), "fonts/times.ttf");
                typefaceSelected = typefaceNew;
                stringTypeface = "Times New Roman";
                imgPicture.setImageBitmap(usingMethodCanvas(inputType, textAyat, typefaceSelected, colorSelection, horizontalAlign, verticalAlign));
            }
            if (item.equalsIgnoreCase("Segoe Ui")){
                Typeface typefaceNew = Typeface.createFromAsset(getAssets(), "fonts/segoeui.ttf");
                typefaceSelected = typefaceNew;
                stringTypeface = "Segoe Ui";
                imgPicture.setImageBitmap(usingMethodCanvas(inputType, textAyat, typefaceSelected, colorSelection, horizontalAlign, verticalAlign));
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                File root = new File(Environment.getExternalStorageDirectory(),"photoAlkitab");
                File f = new File(root.toString());
                for (File temp : f.listFiles()){
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        break;
                    }
                }
                try {
                    Bitmap bitmap=null;
                    BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                    bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(), bitmapOptions);

                    fileTemp = f;

                    imgPicture.setImageBitmap(bitmap);
//                    btnAddAyat.setEnabled(true);
                    BITMAP = bitmap;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getMetadataImages();
            } else if (requestCode == 2) {
                Uri selectedImage = null;
                btnAddAyat.setEnabled(true);
                System.out.println(data.getData()); //getData() = content://media/external/images/media/24457
                String bBrand = "samsung";
                String BBrand = Build.BRAND;
                if (bBrand.equalsIgnoreCase(BBrand)){
                    Uri tempPath=data.getData();
                    System.out.println(getRealPathFromURI(EditPictureActivity.this, tempPath));
                    realPath = getRealPathFromURI(EditPictureActivity.this, tempPath);
                }else {
                    realPath = RealPathUtil.getRealPathFromURI_API19(this, data.getData());
                }

                selectedImage = Uri.fromFile(new File(realPath));
                if (data.getData() != null){
                    source1 = selectedImage;
                }

                Bitmap bitmap = null;
                try {
                    bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                imgPicture.setImageBitmap(bitmap);
                BITMAP = bitmap;
//                getMetadataImages();

            } else if (requestCode==10){
                if (resultCode== Activity.RESULT_OK){
                    textAyat = data.getStringExtra("result");
                    bookName = data.getStringExtra("bookName");
                    book = data.getStringExtra("book");
                    chapter = data.getStringExtra("chapter");
                    verse = data.getStringExtra("verse");

                    radioVerticalTop.setEnabled(true);
                    radioVerticalCenter.setEnabled(true);
                    radioVerticalBottom.setEnabled(true);

                    radioHorizontalLeft.setEnabled(true);
                    radioHorizontalCenter.setEnabled(true);
                    radioHorizontalRight.setEnabled(true);

                    spinnerFontChooser.setEnabled(true);
                    typefaceSelected = Typeface.DEFAULT;
                    imgPicture.setImageBitmap(usingMethodCanvas(inputType, textAyat, typefaceSelected, colorSelection, horizontalAlign, verticalAlign));
                    btnOk.setEnabled(true);
                }
                if (resultCode==Activity.RESULT_CANCELED){

                }
            }
        }
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        // code copied from http://stackoverflow.com/questions/3401579/get-filename-and-path-from-uri-from-mediastore
        // to convert URI to real path for Samsung Device.
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

//    Input Picture
    public void takeAPhoto(View view){
        inputType="camera";
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File root = new File(Environment.getExternalStorageDirectory(), "photoAlkitab");
        if (!root.exists()){
            root.mkdirs();
        }
        File f = new File(root, "temp.jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
        startActivityForResult(intent, 1);
    }

    public void fromGallery(View view){
        inputType="galery";
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, 2);
    }
//    Input Picture

//    Processing Image
    public Bitmap usingMethodCanvas(String type, String text, Typeface typeface,String colorSelection, String horizontalAlign, String verticalAlign){
        Bitmap bm1 = null;
        Bitmap newBitmap = null;
        Bitmap.Config config = null;


        try {
            if (type == "galery") {
                bm1 = BitmapFactory.decodeStream(getContentResolver().openInputStream(source1));
                config = bm1.getConfig();
                if (config==null){
                    config = Bitmap.Config.ARGB_8888;
                }
            }
            else if (type == "camera"){
                BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                bm1 = BitmapFactory.decodeFile(fileTemp.getAbsolutePath(), bitmapOptions);
                config = Bitmap.Config.ARGB_8888;
            }
            else if (type == "edit"){
                bm1=editingBITMAP;

                if (bm1.getConfig()!=null){
                    config = bm1.getConfig();
                }else{
                    config = Bitmap.Config.ARGB_8888;

                }
            }

            newBitmap = Bitmap.createBitmap(bm1.getWidth(), bm1.getHeight(), config);
            Canvas newCanvas = new Canvas(newBitmap);

            int textWidth = newCanvas.getWidth()-10;

            newCanvas.drawBitmap(bm1,0,0,null);

            String captionString = text;
            if (captionString != null){
                TextPaint paintText = new TextPaint(Paint.ANTI_ALIAS_FLAG);
                if (colorSelection == null){
                    colorSelection="#000000";
                }
                paintText.setColor(Color.parseColor(colorSelection));
                paintText.setTextSize(newBitmap.getWidth() * 4 / 100);
                paintText.setTypeface(typeface);

                Rect rectText = new Rect();
                paintText.getTextBounds(captionString, 0, captionString.length(), rectText);

                Layout.Alignment layoutAlignment = null;
                if (horizontalAlign.equalsIgnoreCase("left")){
                    layoutAlignment = Layout.Alignment.ALIGN_NORMAL;
                }else if (horizontalAlign.equalsIgnoreCase("center")){
                    layoutAlignment = Layout.Alignment.ALIGN_CENTER;
                }else if (horizontalAlign.equalsIgnoreCase("right")){
                    layoutAlignment = Layout.Alignment.ALIGN_OPPOSITE;
                }

                StaticLayout textLayout = new StaticLayout(captionString, paintText, textWidth, layoutAlignment, 1.0f, 0.0f, false);
                int textHeight = textLayout.getHeight();

                int x=0, y=0;

                if (horizontalAlign.equalsIgnoreCase("left")){
                    x = 1;
                }else if (horizontalAlign.equalsIgnoreCase("center")){
                    x = (newBitmap.getWidth()-textWidth)/2;
                }else if (horizontalAlign.equalsIgnoreCase("right")){
                    x = newBitmap.getWidth()-textWidth;
                }

                if (verticalAlign.equalsIgnoreCase("top")){
                    y = 1;
                }else if (verticalAlign.equalsIgnoreCase("middle")){
                    y = (newBitmap.getHeight()-textHeight)/2;
                }else if (verticalAlign.equalsIgnoreCase("bottom")){
                    y = (newBitmap.getHeight()-textHeight);
                }

                newCanvas.save();
                newCanvas.translate(x, y);
                textLayout.draw(newCanvas);
                newCanvas.restore();

            }else{
                Toast.makeText(getApplicationContext(), "caption empty!", Toast.LENGTH_LONG).show();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return newBitmap;
    }
//    Processing Image

    public void fileName(){
        File root = new File(Environment.getExternalStorageDirectory(),"Pictures/photoAlkitab");
        if (!root.exists()){
            root.mkdirs();
        }

        String fileName = fileNameNew+"-edit";
        String fileName1 = fileNameNew+"-edit";
        System.out.println("a. fileNameNew : " + fileNameNew + " | fileName : " + fileName + " | fileName1 : " + fileName1);
//        Simple date format to name new image

//        Save file using date. If there are any file with same name, add number after name
        File filePicture = new File(root,fileName+".jpg");
        int num = 0;
        while(filePicture.exists()){
            num++;
            fileName1 = fileName+"-edit("+num+")";
            filePicture = new File(root,fileName1+".jpg");
        }
        fileNameNew = fileName1;
        System.out.println("b. fileNameNew : " + fileNameNew + " | fileName : " + fileName + " | fileName1 : " + fileName1);

//        Save file using date. If there are any file with same name, add number after name
    }

    public void savePicture(){
//        check is there any folder with name = "Pictures"
        File root = new File(Environment.getExternalStorageDirectory(),"Pictures/photoAlkitab");
        if (!root.exists()){
            root.mkdirs();
        }
//        check is there any folder with name = "Pictures"

        File filePicture = new File(root,fileNameNew+".jpg");

        OutputStream outputStream = null;

        try {
            imgPicture.buildDrawingCache();
            Bitmap bitmap = imgPicture.getDrawingCache();
            outputStream = new FileOutputStream(filePicture);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            outputStream.flush();
            outputStream.close();
//            MediaScannerConnection.scanFile(this, new String[]{Environment.getExternalStorageDirectory().toString() + "/Pictures/photoAlkitab"}, null, new MediaScannerConnection.OnScanCompletedListener() {
//                @Override
//                public void onScanCompleted(String path, Uri uri) {
////                    Toast.makeText(EditPictureActivity.this, "Scan Completed", Toast.LENGTH_SHORT).show();
//                }
//            });
            Toast.makeText(EditPictureActivity.this, "Save success", Toast.LENGTH_SHORT).show();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(EditPictureActivity.this, e+"", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(EditPictureActivity.this, e+"", Toast.LENGTH_LONG).show();
        }
    }

    public void buildXMLtoEditPicture(Bitmap picture){
        String stringImage = pictureToString(picture);

        StringBuilder newXML = new StringBuilder();
//        newXML.append(
//                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
//                        "<stringPicture>" + stringImage + "</stringPicture>\n"+
//                        "<stringPassage>" + textAyat + "</stringPassage>\n"+
//                        "<book>" + book + "</book> \n"+
//                        "<chapter>" + chapter + "</chapter> \n"+
//                        "<verse>" + verse + "</verse> \n"+
//                        "<horizontal>" + horizontalAlign + "</horizontal>\n" +
//                        "<vertical>" + verticalAlign + "</vertical>\n" +
//                        "<type>" + stringTypeface + "</type>\n" +
//                        "<color>" + colorSelection + "</color>\n"
//        );
        newXML.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                        "\n" +
                        "<rdf:RDF \n" +
                        "xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n" +
                        "xmlns:imageObject=\"http://schema.org/ImageObject\"\n" +
                        ">\n" +
                        "\n" +

                        "\t<rdf:Description>\n" +
                        "\t\t<imageObject>\n" +
                        "\t\t\t<exif>\n" +
                        "\t\t\t\t<Make>" + tagMake + "</Make>\n" +
                        "\t\t\t\t<Model>" + tagModel + "</Model>\n" +
                        "\t\t\t\t<ExposureTime>" + tagExposureTime + "</ExposureTime>\n" +
                        "\t\t\t\t<FNumber>" + tagAperture + "</FNumber>\n" +
                        "\t\t\t\t<ISO>" + tagISO + "</ISO>\n" +
                        "\t\t\t\t<DateTimeOriginal>" + tagDateTime + "</DateTimeOriginal>\n" +
                        "\t\t\t\t<FocalLength>" + tagFocalLength + "</FocalLength>\n" +
                        "\t\t\t\t<ImageWidht>" + tagImageWidth + "</ImageWidht>\n" +
                        "\t\t\t\t<ImageHeight>" + tagImageLength + "</ImageHeight>\n" +
                        "\t\t\t</exif>\n" +
                        "\t\t\t<imageFile>"+
                        stringImage +
                        "</imageFile>\n" +
                        "\t\t</imageObject>\n" +

                        "\t\t<bible>\n" +
                        "\t\t\t<title>" + book + chapter + ":" + verse + "</title>\n" +
                        "\t\t\t<range>\n" +
                        "\t\t\t\t<request>" + book + chapter + ":" + verse + "</request>\n" +
                        "\t\t\t\t<result>" + bookName + " " + chapter + ":" + verse + "</result>\n" +
                        "\t\t\t\t<item>\n" +
                        "\t\t\t\t\t<book>" + book + "</book>\n" +
                        "\t\t\t\t\t<bookname>" + bookName + "</bookname>\n" +
                        "\t\t\t\t\t<chapter>" + chapter + "</chapter>\n" +
                        "\t\t\t\t\t<verse>" + verse + "</verse>\n" +
                        "\t\t\t\t\t<text>" + textAyat + "</text>\n" +
                        "\t\t\t\t</item>\n" +
                        "\t\t\t</range>\n" +
                        "\t\t\t<font>\n" +
                        "\t\t\t\t<horizontal>" + horizontalAlign + "</horizontal>\n" +
                        "\t\t\t\t<vertical>" + verticalAlign + "</vertical>\n" +
                        "\t\t\t\t<type>" + stringTypeface + "</type>\n" +
                        "\t\t\t\t<color>" + colorSelection + "</color>\n" +
                        "\t\t\t</font>\n" +
                        "\t\t</bible>\n" +

                        "\t</rdf:Description>\n" +
                        "\n" +

                        "</rdf:RDF>"
        );

        try {
            File root = new File(Environment.getExternalStorageDirectory(), "photoAlkitab/source");
            if (!root.exists()){
                root.mkdirs();
            }
            File filepath = new File(root,fileNameNew+".xml");
//            System.out.println(fileNameNew);
            FileWriter writer = new FileWriter(filepath);
            writer.append(newXML);
            writer.flush();
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//Get and Set Metadata
    public void getMetadataImages(){
        try {
            ExifInterface exif=null;
            if (inputType.equalsIgnoreCase("camera")){
                exif = new ExifInterface("/storage/emulated/0/photoAlkitab/temp.jpg");
            }
            else if (inputType.equalsIgnoreCase("galery")){
                exif = new ExifInterface(realPath);
            }
            String myAttribute="Exif information ---\n\n";
            myAttribute += "Camera\n";
            myAttribute += ExifInterface.TAG_MAKE +" : "+ exif.getAttribute(ExifInterface.TAG_MAKE);
            myAttribute += "\n";
            myAttribute += ExifInterface.TAG_MODEL +" : "+ exif.getAttribute(ExifInterface.TAG_MODEL);
            myAttribute += "\n";
            myAttribute += ExifInterface.TAG_ORIENTATION +" : "+ exif.getAttribute(ExifInterface.TAG_ORIENTATION);
            myAttribute += "\n";
            myAttribute += ExifInterface.TAG_EXPOSURE_TIME +" : "+ exif.getAttribute(ExifInterface.TAG_EXPOSURE_TIME);
            myAttribute += "\n";
            myAttribute += ExifInterface.TAG_APERTURE +" : "+ exif.getAttribute(ExifInterface.TAG_APERTURE);
            myAttribute += "\n";
            myAttribute += ExifInterface.TAG_ISO +" : "+ exif.getAttribute(ExifInterface.TAG_ISO);
            myAttribute += "\n";
            myAttribute += ExifInterface.TAG_DATETIME +" : "+ exif.getAttribute(ExifInterface.TAG_DATETIME);
            myAttribute += "\n";
            myAttribute += ExifInterface.TAG_FLASH +" : "+ exif.getAttribute(ExifInterface.TAG_FLASH);
            myAttribute += "\n";
            myAttribute += ExifInterface.TAG_FOCAL_LENGTH +" : "+ exif.getAttribute(ExifInterface.TAG_FOCAL_LENGTH);
            myAttribute += "\n";
            myAttribute += ExifInterface.TAG_IMAGE_LENGTH +" : "+ exif.getAttribute(ExifInterface.TAG_IMAGE_LENGTH);
            myAttribute += "\n";
            myAttribute += ExifInterface.TAG_IMAGE_WIDTH +" : "+ exif.getAttribute(ExifInterface.TAG_IMAGE_WIDTH);
            myAttribute += "\n";
            myAttribute += ExifInterface.TAG_WHITE_BALANCE +" : "+ exif.getAttribute(ExifInterface.TAG_WHITE_BALANCE);
            myAttribute += "\n";

            myAttribute += ExifInterface.TAG_GPS_LATITUDE +" : "+ exif.getAttribute(ExifInterface.TAG_GPS_LATITUDE);
            myAttribute += "\n";
            myAttribute += ExifInterface.TAG_GPS_LATITUDE_REF +" : "+ exif.getAttribute(ExifInterface.TAG_GPS_LATITUDE_REF);
            myAttribute += "\n";
            myAttribute += ExifInterface.TAG_GPS_LONGITUDE +" : "+ exif.getAttribute(ExifInterface.TAG_GPS_LONGITUDE);
            myAttribute += "\n";
            myAttribute += ExifInterface.TAG_GPS_LONGITUDE_REF +" : "+ exif.getAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF);
            myAttribute += "\n";

            if (exif.getAttribute(ExifInterface.TAG_MAKE) != null){
                tagMake = exif.getAttribute(ExifInterface.TAG_MAKE);
            }
            if (exif.getAttribute(ExifInterface.TAG_MODEL) != null){
                tagModel = exif.getAttribute(ExifInterface.TAG_MODEL);
            }
            if (exif.getAttribute(ExifInterface.TAG_ORIENTATION)!=null){
                tagOrientation = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
            }
            if (exif.getAttribute(ExifInterface.TAG_EXPOSURE_TIME)!=null){
                tagExposureTime = exif.getAttribute(ExifInterface.TAG_EXPOSURE_TIME);
            }
            if (exif.getAttribute(ExifInterface.TAG_APERTURE)!=null){
                tagAperture = exif.getAttribute(ExifInterface.TAG_APERTURE);
            }
            if (exif.getAttribute(ExifInterface.TAG_ISO)!=null){
                tagISO = exif.getAttribute(ExifInterface.TAG_ISO);
            }
            if (exif.getAttribute(ExifInterface.TAG_DATETIME)!=null){
                tagDateTime = exif.getAttribute(ExifInterface.TAG_DATETIME);
            }
            if (exif.getAttribute(ExifInterface.TAG_FLASH)!=null){
                tagFlash = exif.getAttribute(ExifInterface.TAG_FLASH);
            }
            if (exif.getAttribute(ExifInterface.TAG_FOCAL_LENGTH)!=null) {
                tagFocalLength = exif.getAttribute(ExifInterface.TAG_FOCAL_LENGTH);
            }
            if (exif.getAttribute(ExifInterface.TAG_IMAGE_LENGTH)!=null) {
                tagImageLength = exif.getAttribute(ExifInterface.TAG_IMAGE_LENGTH);
            }
            if (exif.getAttribute(ExifInterface.TAG_IMAGE_WIDTH)!=null) {
                tagImageWidth = exif.getAttribute(ExifInterface.TAG_IMAGE_WIDTH);
            }
            if (exif.getAttribute(ExifInterface.TAG_WHITE_BALANCE)!=null) {
                tagWhiteBalance = exif.getAttribute(ExifInterface.TAG_WHITE_BALANCE);
            }

            if (exif.getAttribute(ExifInterface.TAG_GPS_LATITUDE)!=null) {
                tagGPSLatitude = exif.getAttribute(ExifInterface.TAG_GPS_LATITUDE);
            }
            if (exif.getAttribute(ExifInterface.TAG_GPS_LATITUDE_REF)!=null) {
                tagGPSLatitudeRef = exif.getAttribute(ExifInterface.TAG_GPS_LATITUDE_REF);
            }
            if (exif.getAttribute(ExifInterface.TAG_GPS_LONGITUDE)!=null) {
                tagGPSLongitude = exif.getAttribute(ExifInterface.TAG_GPS_LONGITUDE);
            }
            if (exif.getAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF)!=null) {
                tagGPSLongitudeRef = exif.getAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF);
            }

//            myAttribute += "\n"+realPath;
//            txtContoh.setText(myAttribute);
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    public void writeEXIFtoImage(String ImageName){
//        Write EXIF to New Picture

        String name = null;
        name = "/storage/emulated/0/Pictures/photoAlkitab/"+ImageName+".jpg";

        try {
            ExifInterface exif = new ExifInterface(name);
            exif.setAttribute(ExifInterface.TAG_MAKE, tagMake);
            exif.setAttribute(ExifInterface.TAG_MODEL, tagModel);
            exif.setAttribute(ExifInterface.TAG_ORIENTATION, tagOrientation);
            exif.setAttribute(ExifInterface.TAG_EXPOSURE_TIME, tagExposureTime);
            exif.setAttribute(ExifInterface.TAG_APERTURE, tagAperture);
            exif.setAttribute(ExifInterface.TAG_ISO, tagISO);
            exif.setAttribute(ExifInterface.TAG_DATETIME, tagDateTime);
            exif.setAttribute(ExifInterface.TAG_FLASH, tagFlash);
            exif.setAttribute(ExifInterface.TAG_FOCAL_LENGTH, tagFocalLength);
            exif.setAttribute(ExifInterface.TAG_IMAGE_LENGTH, tagImageLength);
            exif.setAttribute(ExifInterface.TAG_IMAGE_WIDTH, tagImageWidth);
            exif.setAttribute(ExifInterface.TAG_WHITE_BALANCE, tagWhiteBalance);

            exif.saveAttributes();

        } catch (IOException e) {
            e.printStackTrace();
        }
//        Write EXIF to New Picture
    }
//Get and Set Metadata

//    Make New XML
    public void writeXML(ImageView images){
        String base64Picture=null;
        String digest = null;
        String stringSignature = null;
        StringBuilder newXML = new StringBuilder();

        base64Picture = pictureToString(images);
        digest = messageDigest(base64Picture);
        stringSignature = digitalSignature(digest);

        newXML.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                        "\n" +
                        "<rdf:RDF \n" +
                        "xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n" +
                        "xmlns:imageObject=\"http://schema.org/ImageObject\"\n" +
                        ">\n" +
                        "\n" +

                        "\t<rdf:Description>\n" +
                        "\t\t<imageObject>\n" +
                        "\t\t\t<exif>\n" +
                        "\t\t\t\t<Make>" + tagMake + "</Make>\n" +
                        "\t\t\t\t<Model>" + tagModel + "</Model>\n" +
                        "\t\t\t\t<ExposureTime>" + tagExposureTime + "</ExposureTime>\n" +
                        "\t\t\t\t<FNumber>" + tagAperture + "</FNumber>\n" +
                        "\t\t\t\t<ISO>" + tagISO + "</ISO>\n" +
                        "\t\t\t\t<DateTimeOriginal>" + tagDateTime + "</DateTimeOriginal>\n" +
                        "\t\t\t\t<FocalLength>" + tagFocalLength + "</FocalLength>\n" +
                        "\t\t\t\t<ImageWidht>" + tagImageWidth + "</ImageWidht>\n" +
                        "\t\t\t\t<ImageHeight>" + tagImageLength + "</ImageHeight>\n" +
                        "\t\t\t</exif>\n" +
                        "\t\t\t<imageFile>\n" +
                        "\t\t\t\t" + base64Picture + "\n" +
//                        "\t\t\t\t" + "a" + "\n" +
                        "\t\t\t</imageFile>\n" +
                        "\t\t</imageObject>\n" +

                        "\t\t<bible>\n" +
                        "\t\t\t<title>" + book + chapter + ":" + verse + "</title>\n" +
                        "\t\t\t<range>\n" +
                        "\t\t\t\t<request>" + book + chapter + ":" + verse + "</request>\n" +
                        "\t\t\t\t<result>" + bookName + " " + chapter + ":" + verse + "</result>\n" +
                        "\t\t\t\t<item>\n" +
                        "\t\t\t\t\t<book>" + book + "</book>\n" +
                        "\t\t\t\t\t<bookname>" + bookName + "</bookname>\n" +
                        "\t\t\t\t\t<chapter>" + chapter + "</chapter>\n" +
                        "\t\t\t\t\t<verse>" + verse + "</verse>\n" +
                        "\t\t\t\t\t<text>" + textAyat + "</text>\n" +
                        "\t\t\t\t</item>\n" +
                        "\t\t\t</range>\n" +
                        "\t\t\t<font>\n" +
                        "\t\t\t\t<horizontal>" + horizontalAlign + "</horizontal>\n" +
                        "\t\t\t\t<vertical>" + verticalAlign + "</vertical>\n" +
                        "\t\t\t\t<type>" + stringTypeface + "</type>\n" +
                        "\t\t\t\t<color>" + colorSelection + "</color>\n" +
                        "\t\t\t</font>\n" +
                        "\t\t</bible>\n" +

                        "\t</rdf:Description>\n" +
                        "\n" +

                        "\t<Signature xmlns:Signature=\"http://www.w3.org/2000/09/xmldsig#\">\n" +
                        "\t\t<SignedInfo>\n" +
                        "\t\t\t<CanonicalizationMethod Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\"/> \n" +
                        "\t\t\t<SignatureMethod Algoritm=\"http://www.w3.org/2000/09/xmldsig#rsa-sha1\"/>\n" +
                        "\t\t\t<Reference URI=\"http://www.w3.org/TR/2000/REC-xhtml1-20000126/\">\n" +
                        "\t\t\t\t<Transforms>\n" +
                        "\t\t\t\t\t<Transform Algorithm=\"http://www.w3.org/2000/09/xmldsig#enveloped-signature\"/>\n" +
                        "\t\t\t\t</Transforms>\n" +
                        "\t\t\t\t<DigestMethod Algorithm=\"http://www.w3.org/2001/04/xmldsig-more#md5\"/>\n" +
                        "\t\t\t\t<DigestValue>\n" +
                        "\t\t\t\t\t" + digest + "\n" +
                        "\t\t\t\t</DigestValue>\n" +
                        "\t\t\t</Reference>\n" +
                        "\t\t</SignedInfo>\n" +
                        "\t\t<SignatureValue>\n" +
                        "\t\t\t" + stringSignature + "\n" +
                        "\t\t</SignatureValue>\n" +
                        "\t</Signature>\n" +
                        "</rdf:RDF>"
        );
//        tmpNewXML=newXML.toString();
        tmpStringBuilder = newXML;


        try {
            File root = new File(Environment.getExternalStorageDirectory(), "photoAlkitab");
            if (!root.exists()){
                root.mkdirs();
            }
            File filepath = new File(root,"newXML-fix.xml");
            FileWriter writer = new FileWriter(filepath);
            writer.append(newXML);
            writer.flush();
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
//        Toast.makeText(AddPhotoActivity.this, ""+response.toString(), Toast.LENGTH_SHORT).show();
    }

    public String pictureToString(ImageView iv){
        iv.buildDrawingCache();
        Bitmap bitmap = iv.getDrawingCache();

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteFormat = stream.toByteArray();

        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);
        return imgString;
    }

    public String pictureToString(Bitmap bitmapInput){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmapInput.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteFormat = stream.toByteArray();

        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);
        return imgString;
    }

    public String messageDigest(String stockString){
        //        Diambil dan dimodifikasi dari http://stackoverflow.com/questions/5980658/how-to-sha1-hash-a-string-in-android
        StringBuilder stringBuilder = new StringBuilder();
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            md.update(stockString.getBytes(),0,stockString.length());
            byte[] sha1hash = md.digest();

            for (byte b : sha1hash){
                int halfbyte = (b >>> 4) & 0x0F;
                int two_halfs = 0;
                do {
                    stringBuilder.append((0 <= halfbyte)&&(halfbyte <= 9) ? (char)('0'+halfbyte) : (char)('a'+(halfbyte-10)));
                    halfbyte = b & 0x0F;
                } while (two_halfs++ < 1);
            }

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }

    private String digitalSignature(String base64String){
        byte[] privateKeyBytes;
        privateKeyBytes = Base64.decode(sharedPreferences.getString(PRIVATEKEY, null), 0);
        String signatureFix = null;

        KeyFactory kf = null;
        try {
            kf = KeyFactory.getInstance("RSA");
            PrivateKey pk = kf.generatePrivate(new PKCS8EncodedKeySpec(privateKeyBytes));

            Signature signature = Signature.getInstance("SHA1WithRSA");
            signature.initSign(pk);

            byte[] bytes = Base64.decode(base64String,0);
            signature.update(bytes);
            byte[] signatureByte = signature.sign();

            signatureFix = new String(Base64.encode(signatureByte,0));

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            e.printStackTrace();
        }
        return signatureFix;
    }
//    Make New XML

    private class SendToWebServiceAsync extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(EditPictureActivity.this);
            pDialog.setMessage("Loading...");
            pDialog.show();
//            Toast.makeText(AddPhotoActivity.this, sharedPreferences.getString(USERNAME,null)+" "+sharedPreferences.getString(TOKEN,null), Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
//            refreshToken();
            pDialog.dismiss();
//            Toast.makeText(AddPhotoActivity.this, ""+strResponse, Toast.LENGTH_SHORT).show();
//            System.out.println(response);
            System.out.println(strResponse);

            String newToken = null;
            try {
                JSONObject json = new JSONObject(strResponse);
                newToken = json.getString("token");
                System.out.println(json.getString("hasil")+" | "+json.getString("token"));
                Toast.makeText(EditPictureActivity.this, json.getString("hasil")+" | "+json.getString("token"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                e.printStackTrace();
            }
//
            editor.putString(TOKEN, newToken);
        }

        @Override
        protected Void doInBackground(Void... params) {
            request = new SoapObject("","validateXMLSignature");
            request.addProperty("XMLSignature", tmpStringBuilder.toString());
            request.addProperty("username", sharedPreferences.getString(USERNAME, null));
            request.addProperty("token", sharedPreferences.getString(TOKEN,null));
            System.out.println("username = " + sharedPreferences.getString(USERNAME, null) + "\n token = " + sharedPreferences.getString(TOKEN, null));

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet=true;
            envelope.setOutputSoapObject(request);
//            HttpTransportSE httpTransportSE = new HttpTransportSE("http://192.168.137.21:8080/DS/server.php");
//            HttpTransportSE httpTransportSE = new HttpTransportSE("http://192.168.137.1:8080/photoalkitabserver/server.php");
            HttpTransportSE httpTransportSE = new HttpTransportSE("http://vhost.ti.ukdw.ac.id/~nanda/server.php");

            try {
//                httpTransportSE.call("http://192.168.137.1:8080/photoalkitabserver/server.php/validateXMLSignature", envelope);
                httpTransportSE.call(" http://vhost.ti.ukdw.ac.id/~nanda/server.php/validateXMLSignature", envelope);
//                response = (SoapPrimitive)envelope.getResponse();
                strResponse = (String)envelope.getResponse();
//                System.out.println(strResponse);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    public void loadPassage(String book, String chapter, String verse){

        String urlAddress  = "https://bibles.org/v2/passages.js?q[]="+book+"+"+chapter+":"+verse+"&version=eng-GNTD";

        final String username = "cKaMAGU4fL3kc4w5zMZbvoPvsmOJEpTjyFUjXeab";
        final String passwd = "x";

        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, urlAddress, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                String text = null;
                String stringNormalization2 = null;
                try {
                    JSONObject jsonResponse = response.getJSONObject("response");
                    JSONObject jsonSearch = jsonResponse.getJSONObject("search");
                    JSONObject jsonResult = jsonSearch.getJSONObject("result");
                    JSONArray jsonPassages = jsonResult.getJSONArray("passages");

                    for (int i = 0; i < jsonPassages.length(); i++){
                        JSONObject passages = jsonPassages.getJSONObject(i);
                        text = passages.getString("text");
                    }
                    stringNormalization2 = Html.fromHtml(text).toString();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                textAyat = stringNormalization2;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String responseOrError = error.getMessage();
                Toast.makeText(EditPictureActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                return params;
            }

            @Override
            public Map<String, String> getHeaders(){
                Map<String,String> headers = new HashMap<String, String>();
                String credentials = username+":"+passwd;
                String auth = "Basic " + Base64.encodeToString(credentials.getBytes(),Base64.DEFAULT).replace("\n","");
                headers.put("Authorization", auth);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonObjectRequest);
    }

    public void getBookName(String book){
        String urlAddress = "https://bibles.org/v2/versions/eng-GNTD/books.js";
        final Map<String, String> listBooks= new HashMap<>();

        final String username = "cKaMAGU4fL3kc4w5zMZbvoPvsmOJEpTjyFUjXeab";
        final String passwd = "x";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, urlAddress, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONObject jsonResponse = response.getJSONObject("response");
                    JSONArray jsonBooks = jsonResponse.getJSONArray("books");
                    for (int i = 0; i<jsonBooks.length(); i++){
                        JSONObject jsonBook = jsonBooks.getJSONObject(i);
                        listBooks.put(jsonBook.getString("abbr"), jsonBook.getString("name"));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String responseOrError = error.getMessage();
                Toast.makeText(EditPictureActivity.this, "Error\n"+error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                return params;
            }

            @Override
            public Map<String, String> getHeaders(){
                Map<String,String> headers = new HashMap<String, String>();
                String credentials = username+":"+passwd;
                String auth = "Basic " + Base64.encodeToString(credentials.getBytes(),Base64.DEFAULT).replace("\n","");
                headers.put("Authorization", auth);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonObjectRequest);
        bookName = listBooks.get(book);
//        Runnable runnable = new Runnable() {
//            @Override
//            public void run() {
//                imgPicture.setImageBitmap(usingMethodCanvas(inputType, textAyat, typefaceSelected, colorSelection, horizontalAlign, verticalAlign));
//            }
//        };
//        Thread thread = new Thread(runnable);
//        thread.start();
//        try {
//            thread.join();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }
}
