package com.krisananta.adhya.photoalkitab;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;

public class TestPictureActivity extends AppCompatActivity {

    Button btnCamera, btnGalery;
    TextView lblTextExample;
    ImageView imgExample;
    Bitmap imagetmp;
    String typeInput = null;

    Uri source1=null;

    File fileTemp = new File(Environment.getExternalStorageDirectory().toString());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_picture);

        imgExample = (ImageView)findViewById(R.id.imgExample);
        imgExample.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgExample.setImageBitmap(
                        usingMethodCanvas(
                                typeInput,
                                lblTextExample.getText().toString()
                        )
                );
            }
        });

        btnCamera = (Button)findViewById(R.id.btnCamera);
        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraFunction();
                typeInput = "camera";
//                imgExample.setImageBitmap(usingMethodCanvas(((BitmapDrawable) imgExample.getDrawable()).getBitmap(), lblTextExample.getText().toString()));
            }
        });
        btnGalery = (Button)findViewById(R.id.btnGalery);
        btnGalery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnGaleryFunction();
                typeInput = "galery";
            }
        });

        lblTextExample = (TextView)findViewById(R.id.lblTextExample);
        lblTextExample.setText("Disini dicoba dituliskan salah satu ayat kitab suci");
        Typeface typefaceNew = Typeface.createFromAsset(getAssets(), "fonts/Helvetica.ttf");
        lblTextExample.setTypeface(typefaceNew);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        Toast.makeText(TestPictureActivity.this, requestCode + " | " + resultCode + " | " + data, Toast.LENGTH_SHORT).show();

        if (requestCode == 1) {
            File root = new File(Environment.getExternalStorageDirectory(),"photoAlkitab");
            File f = new File(root.toString());
//            File f = new File(Environment.getExternalStorageDirectory().toString());
            for (File temp : f.listFiles()){
                if (temp.getName().equals("temp.jpg")) {
                    f = temp;
                    break;
                }
            }
            try {
                Bitmap bitmap;
                BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(), bitmapOptions);

                fileTemp = f;

                imgExample.setImageBitmap(bitmap);

//                String path = android.os.Environment.getExternalStorageDirectory() + File.separator + "Phoenix" + File.separator + "default";

//                f.delete();

//                OutputStream outFile = null;

//                File file = new File(path, String.valueOf(System.currentTimeMillis()) + ".jpg");
//
//                try {
//                    outFile = new FileOutputStream(file);
//                    bitmap.compress(Bitmap.CompressFormat.JPEG, 85, outFile);
//                    outFile.flush();
//                    outFile.close();
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
            } catch (Exception e) {
                e.printStackTrace();
            }
//            Bundle extras = data.getExtras();
//            Bitmap imageBitmap = (Bitmap)extras.get("data");
//            imgExample.setImageBitmap(imageBitmap);
        } else if (requestCode == 2) {
            Uri selectedImage = null;
            if (data.getData() != null){
                selectedImage = data.getData();
                source1 = selectedImage;
            }
            String[] filePath = { MediaStore.Images.Media.DATA };
            Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
            c.moveToFirst();
            int columnIndex = c.getColumnIndex(filePath[0]);
            String picturePath = c.getString(columnIndex);
            c.close();
            Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
            imagetmp = thumbnail;
            imgExample.setImageBitmap(thumbnail);
        }
//
    }

    public void cameraFunction(){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        File f = new File(android.os.Environment.getExternalStorageDirectory(), "photoAlkitab/temp.jpg");
//        File f = new File(android.os.Environment.getExternalStorageDirectory()+"photoAlkitab", "temp.jpg");
        File root = new File(Environment.getExternalStorageDirectory(), "photoAlkitab");
        File f = new File(root, "temp.jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
        startActivityForResult(intent, 1);
//        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        if (intent.resolveActivity(getPackageManager())!=null){
//            File photofile = null;
//            try {
//                photofile = File.createTempFile("example picture",".jpg",Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES));
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            if (photofile!=null){
//                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photofile));
//                startActivityForResult(intent, 1);
//            }
//        }
    }

    public void btnGaleryFunction(){
        Intent intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, 2);
    }

    public Bitmap usingMethodCanvas(String type, String text){
        Bitmap bm1 = null;
        Bitmap newBitmap = null;
        Bitmap.Config config = null;

        try {
            if (type == "galery") {
                bm1 = BitmapFactory.decodeStream(getContentResolver().openInputStream(source1));
                config = bm1.getConfig();
                if (config==null){
                    config = Bitmap.Config.ARGB_8888;
                }
            }
            else if (type == "camera"){
                BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                bm1 = BitmapFactory.decodeFile(fileTemp.getAbsolutePath(), bitmapOptions);
                config = Bitmap.Config.ARGB_8888;
            }

            newBitmap = Bitmap.createBitmap(bm1.getWidth(), bm1.getHeight(), config);
            Canvas newCanvas = new Canvas(newBitmap);

            newCanvas.drawBitmap(bm1,0,0,null);

            String captionString = text;
            if (captionString != null){
                Paint paintText = new Paint(Paint.ANTI_ALIAS_FLAG);
                paintText.setColor(Color.BLUE);
                paintText.setTextSize(newBitmap.getWidth() * 4 / 100);
                paintText.setTextAlign(Paint.Align.CENTER);
//                paintText.setStyle(Paint.Style.FILL);
//                paintText.setShadowLayer(10f, 10f, 10f, Color.BLACK);

                Rect rectText = new Rect();
                paintText.getTextBounds(captionString, 0, captionString.length(), rectText);

                newCanvas.drawText(captionString, newBitmap.getWidth() / 2, (newBitmap.getHeight()/2), paintText);
            }else{
                Toast.makeText(getApplicationContext(), "caption empty!", Toast.LENGTH_LONG).show();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return newBitmap;
    }
}
