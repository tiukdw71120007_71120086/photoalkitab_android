package com.krisananta.adhya.photoalkitab;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ListEditPictureActivity extends AppCompatActivity {

    private LinearLayout linearLayoutListEdit;
    private TextView titlePicture;
    private Get_Internal_Data getData = new Get_Internal_Data();
    private ArrayList<String> listTitle, arrayValueFile;
    private ListView lv_ListData;

    private String[] coba={"a","b","c"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_edit_picture);

        listTitle = getData.getListData();

        ArrayAdapter adapter = new ArrayAdapter<String>(ListEditPictureActivity.this, R.layout.activity_listview,listTitle);
        for (int i = 0; i< adapter.getCount(); i++) {
//            System.out.println(adapter.getItem(i));
        }
        lv_ListData = (ListView)findViewById(R.id.lv_ListData);
        lv_ListData.setAdapter(adapter);
        lv_ListData.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                ProgressDialog progressDialog = new ProgressDialog(ListEditPictureActivity.this);
                progressDialog.setMessage("Loading...");
                progressDialog.show();
//                System.out.println(listTitle.get(position));
//                Toast.makeText(ListEditPictureActivity.this, position+", "+id, Toast.LENGTH_SHORT).show();

                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        arrayValueFile = getData.getSelectedData(listTitle.get(position));
                        for (int i = 0; i<arrayValueFile.size();i++){
//                            System.out.println(arrayValueFile.get(i));
                        }

                    }
                };
                Thread thread = new Thread(runnable);
                thread.start();
                try {
                    thread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(ListEditPictureActivity.this, EditPictureActivity.class);
//                intent.putExtra("FILE NAME", )
                intent.putExtra("ARRAY SELECTED FILE", listTitle.get(position));
                progressDialog.dismiss();

                startActivity(intent);
            }
        });
    }
}
