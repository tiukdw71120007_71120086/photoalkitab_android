package com.krisananta.adhya.photoalkitab;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

public class SignInActivity extends AppCompatActivity {

    Button btnLogin;
    EditText txtUsername, txtPassword;
    TextView lblDaftar;
    private String username="", password="";

    TextView lblPrivKey;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    public static final String MyPREFERENCES = "MyPrefs";
    public static final String Id = "id";
    public static final String Username = "username";
    public static final String Password = "password";
    public static final String PrivateKey = "privateKey";
    public static final String TOKEN = "token";

//    public static final String URL_ADDRESS = "http://192.168.137.1:8080/photoalkitabserver/login.php";
    public static final String URL_ADDRESS = "http://vhost.ti.ukdw.ac.id/~nanda/login.php";

//    public static final String URL_ADDRESS = "http://192.168.1.20:8080/photoalkitabserver/login.php";
//    public static final String URL_ADDRESS = "http://169.254.219.32:8080/photoalkitabserver/login.php";

    static Activity signInActivity;

    StringBuilder text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        btnLogin = (Button)findViewById(R.id.btnLogIn);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               signIn();
            }
        });
        txtUsername = (EditText)findViewById(R.id.txtUsername);
        txtPassword = (EditText)findViewById(R.id.txtPassword);
        lblDaftar = (TextView)findViewById(R.id.lblDaftar);

        lblPrivKey = (TextView)findViewById(R.id.lblPrivKey);

        sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

//        signInActivity=this;
    }

    public static Activity getInstance(){
        return signInActivity;
    }

    public void goToSignUp(View view){
        Intent signUp = new Intent(this, SignUpActivity.class);
        startActivity(signUp);
    }

    public void signIn(){
        username = txtUsername.getText().toString();
        password = txtPassword.getText().toString();

//        Toast.makeText(SignInActivity.this, username, Toast.LENGTH_SHORT).show();

        signInUser();
//        signInToNetwork();

//        if(username.equals("admin") && password.equals("admin")){
//
//            editor.putString(Username, username);
//            editor.putString(Password, password);
//            editor.commit();
//
//            startActivity(new Intent(this, MainActivity.class));
//        }else{
//            Toast.makeText(this, "Username dan Password Tidak terdaftar", Toast.LENGTH_LONG).show();
//        }

    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void signInToNetwork(){
        HashMap<String, String> params = new HashMap<>();
        params.put("username", username);
        params.put("password",password);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(URL_ADDRESS, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(SignInActivity.this, "Success", Toast.LENGTH_SHORT).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            Toast.makeText(SignInActivity.this, "Time Out Error", Toast.LENGTH_SHORT).show();
                        } else if (error instanceof AuthFailureError) {
                            //TODO
                            Toast.makeText(SignInActivity.this, "AuthFailureError", Toast.LENGTH_SHORT).show();
                        } else if (error instanceof ServerError) {
                            //TODO
                            Toast.makeText(SignInActivity.this, "Server Error", Toast.LENGTH_SHORT).show();
                        } else if (error instanceof NetworkError) {
                            //TODO
                            Toast.makeText(SignInActivity.this, "Network Error", Toast.LENGTH_SHORT).show();
                        } else if (error instanceof ParseError) {
                            //TODO
                            Toast.makeText(SignInActivity.this, "Parse Error", Toast.LENGTH_SHORT).show();
                        }
//                        Toast.makeText(SignInActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("username", username);
                params.put("password",password);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonRequest);
//        Volley.newRequestQueue(this).add(jsonRequest);
//        SignInActivity.getInstance().addR
    }

    public void signInUser(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_ADDRESS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        showJSON(response);
//                        Toast.makeText(SignInActivity.this, response, Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(SignInActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("username", username);
                params.put("password", password);
//                Toast.makeText(SignInActivity.this, username, Toast.LENGTH_SHORT).show();
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void showJSON(String json) {
        ParseJSONUser pj = new ParseJSONUser(json);
        pj.parseJSON();
//        Toast.makeText(SignInActivity.this, username+", "+password, Toast.LENGTH_SHORT).show();
        Toast.makeText(SignInActivity.this, ParseJSONUser.getIds() + ", " + ParseJSONUser.getUsernames() + ", " + ParseJSONUser.getTokens(), Toast.LENGTH_LONG).show();
//        Toast.makeText(SignInActivity.this, pj.toString(), Toast.LENGTH_SHORT).show();

        if (pj.getIds()==null){

        }else {
            readPrivateKey(pj.getUsernames());

            Intent intent = new Intent(SignInActivity.this, MainActivity.class);
            editor.putString(Id, pj.getIds());
            editor.putString(Username, pj.getUsernames());
            editor.putString(PrivateKey, text.toString());
            editor.putString(TOKEN, pj.getTokens());
            editor.commit();
            startActivity(intent);

            finish();
        }
    }

    public void readPrivateKey(String filename){
        File root = new File(Environment.getExternalStorageDirectory(),"photoAlkitab");
        File file = new File(root,filename+".txt");

        text = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine())!=null){
                text.append(line);
                text.append('\n');
            }
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

//        lblPrivKey.setText(text.toString());
    }
}
