package com.krisananta.adhya.photoalkitab;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SignUpActivity extends AppCompatActivity {

    Button btnDaftar;
    EditText txtNama, txtUsername, txtEmail, txtPassword, txtPassword2;
    String nama, username, email, password, password2;


    PublicKey pubKey;
    PrivateKey privKey;
    KeyPairGenerator generator;
    byte[] publicKeyBytes, privKeyBytes;
    String pubKeyStr, privKeyStr;

//    public static final String URL_ADDRESS = "http://192.168.137.1:8080/photoalkitabserver/adduser.php";
    public static final String URL_ADDRESS = "http://vhost.ti.ukdw.ac.id/~nanda/adduser.php";
//    public static final String URL_ADDRESS = "http://192.168.1.20:8080/photoalkitabserver/adduser.php";
//    public static final String URL_ADDRESS = "http://169.254.219.32:8080/photoalkitabserver/adduser.php";

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    public static final String MyPREFERENCES = "MyPrefs";
    public static final String Id = "id";
    public static final String Username = "username";
    public static final String Password = "password";
    public static final String PRIVATEKEY = "privateKey";
    public static final String TOKEN = "token";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        txtNama = (EditText)findViewById(R.id.txtNama);
        txtEmail = (EditText)findViewById(R.id.txtEmail);
        txtUsername = (EditText)findViewById(R.id.txtUsername);
        txtPassword = (EditText)findViewById(R.id.txtPassword);
        txtPassword2 = (EditText)findViewById(R.id.txtPassword2);

        btnDaftar = (Button)findViewById(R.id.btnDaftar);
        btnDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                daftar();
            }
        });

        sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public void daftar(){
        nama = txtNama.getText().toString();
        username = txtUsername.getText().toString();
        email = txtEmail.getText().toString();
        password = txtPassword.getText().toString();
        password2 = txtPassword2.getText().toString();

        if (password.equals(password2)){
            generateKeys();
            daftarToDatabase(nama, username, email, password);


        }else{
            Toast.makeText(SignUpActivity.this, "Password Tidak Cocok", Toast.LENGTH_SHORT).show();
        }
    }

    protected void daftarToDatabase(final String nama, final String username, final String email, final String password){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_ADDRESS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        saveToTxt();
//                        Toast.makeText(SignUpActivity.this,"User telah didaftarkan",Toast.LENGTH_LONG).show();
//
//                        Intent intent = new Intent(SignUpActivity.this, MainActivity.class);
//                        editor.putString(Username, username);
//                        editor.putString(Password, password);
//                        editor.putString(PRIVATEKEY, privKeyStr);
//                        editor.commit();
//                        startActivity(intent);

                        ParseJSONUser pj = new ParseJSONUser(response);
                        pj.parseJSON();

                        Toast.makeText(SignUpActivity.this, pj.getIds() + " " + pj.getUsernames() + " " + pj.getTokens(), Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(SignUpActivity.this, MainActivity.class);
                        editor.putString(Id, pj.getIds());
                        editor.putString(Username, pj.getUsernames());
                        editor.putString(PRIVATEKEY, privKeyStr);
                        editor.putString(TOKEN, pj.getTokens());
                        editor.commit();
                        startActivity(intent);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(SignUpActivity.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
             protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("name",nama);
                params.put("username",username);
                params.put("password",password);
                params.put("email", email);
                params.put("publicKey", pubKeyStr);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public void generateKeys(){
        try {
            generator = KeyPairGenerator.getInstance("RSA");
            generator.initialize(1024);
            KeyPair pair = generator.generateKeyPair();
            pubKey = pair.getPublic();
            privKey = pair.getPrivate();

            publicKeyBytes = pubKey.getEncoded();
            privKeyBytes = privKey.getEncoded();

            pubKeyStr = new String(Base64.encode(publicKeyBytes,0));
            privKeyStr = new String(Base64.encode(privKeyBytes,0));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public void saveToTxt(){
        try {
            File root = new File(Environment.getExternalStorageDirectory(),"photoAlkitab");
            if (!root.exists()){
                root.mkdirs();
            }
            File filepath = new File(root,username+".txt");
            FileWriter writer = new FileWriter(filepath);
            writer.append(privKeyStr);
            writer.flush();
            writer.close();
//            Toast.makeText(SignUpActivity.this, "Success", Toast.LENGTH_SHORT).show();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
//            Toast.makeText(SignUpActivity.this, "FileNotFoundException", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
//            Toast.makeText(SignUpActivity.this, "IOException", Toast.LENGTH_SHORT).show();
        }
    }
}
