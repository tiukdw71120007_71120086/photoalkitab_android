package com.krisananta.adhya.photoalkitab;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

public class MainActivity extends AppCompatActivity {

    Button btnAddPicture, btnLogOut, btnTestPicture, btnEditPicture;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    public static final String MyPREFERENCES = "MyPrefs";
    public static final String Id = "id";
    public static final String Username = "username";
    public static final String Password = "password";
    public static final String PrivateKey = "privateKey";

    Button btnTestXML;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (SignInActivity.getInstance()==null){

        }else{
            SignInActivity.getInstance().finish();
        }

        btnAddPicture = (Button)findViewById(R.id.btnAddPicture);
        btnAddPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentAddPicture();
            }
        });

        btnLogOut = (Button)findViewById(R.id.btnLogOut);
        btnLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logOut();
            }
        });

        btnEditPicture = (Button)findViewById(R.id.btnEditPicture);
        btnEditPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Get_Internal_Data getData = new Get_Internal_Data();
                if (getData.getSizeData()>0){
                    Toast.makeText(MainActivity.this, ""+getData.getSizeData(), Toast.LENGTH_SHORT).show();
                    System.out.println(getData.getSizeData());
                    Intent intent = new Intent(MainActivity.this, ListEditPictureActivity.class);
                    startActivity(intent);
                }else{
                    Toast.makeText(MainActivity.this, "Data Kosong...", Toast.LENGTH_SHORT).show();
                }

//                Intent intent = new Intent(MainActivity.this, ListEditPictureActivity.class);
//                startActivity(intent);
            }
        });

        sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        String value = sharedPreferences.getString(Username, null);

        if(value == null){
            Intent intent = new Intent(this, SignInActivity.class);
            startActivity(intent);
        }

        btnTestXML = (Button)findViewById(R.id.btnTestXML);

        btnTestXML.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, TestXMLActivity.class);
                startActivity(intent);
            }
        });

        btnTestPicture = (Button)findViewById(R.id.btnTestPicture);
        btnTestPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(MainActivity.this, TestPictureActivity.class);
                Intent intent = new Intent(MainActivity.this, TestSOAP.class);
                startActivity(intent);
            }
        });
    }

    public void intentAddPicture(){
        Intent intent = new Intent(this, AddPhotoActivity.class);
        startActivity(intent);
    }

    public void logOut(){
//        String URL_ADDRESS = "http://192.168.137.1:8080/photoalkitabserver/logout.php";
        String URL_ADDRESS = "http://vhost.ti.ukdw.ac.id/~nanda/logout.php";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_ADDRESS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(MainActivity.this, "" + response, Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }){

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

        editor.clear();
        editor.commit();
        finish();
        System.exit(0);

//        Intent intent = new Intent(this, SignInActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        startActivity(intent);
    }

    @Override
    public void onBackPressed(){
        String value = sharedPreferences.getString(Username, null);

        if(value != null){
            super.onBackPressed();

            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }
}
